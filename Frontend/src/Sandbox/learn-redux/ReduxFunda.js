import React, {Component} from 'react';
import {createStore} from 'redux';

const initialState = {
  salary: 15000,
  salaryChange: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD':
      state = {
        ...state,
        salary: state.salary += action.payload,
        salaryChange: [...state.salaryChange, action.payload]
      };
      break;
    case 'SUBTRACT':
      state = {
        ...state,
        salary: state.salary -= action.payload,
        salaryChange: [...state.salaryChange, action.payload]
      };
      break;
    default:
  }
  return state;
};

const testRedux = () => {
  const store = createStore(reducer);
  store.subscribe(() => console.log("Update store: ", store.getState()));

  store.dispatch({type: 'ADD', payload: 15000});
  store.dispatch({type: 'ADD', payload: 15000});
  store.dispatch({type: 'ADD', payload: 30000});
  store.dispatch({type: 'SUBTRACT', payload: 8000});
};

class ReduxFunda extends Component {
  componentDidMount() {
    testRedux();
  }

  render() {
    return (
        <div>
          <h6>Redux Fundamental Learning</h6>
          See result in console.
        </div>
    );
  }
}

export default ReduxFunda;
