import React, {Component} from 'react';
import {createStore} from 'redux';

class IntegrateReactRedux extends Component {

  initSalaryState = {
    salary: 15000,
    salaryChange: []
  };

  reducer = (state = this.initSalaryState, action) => {
    switch (action.type) {
      case 'INCREASE':
        state = {
          ...state,
          salary: state.salary += action.payload,
          salaryChange: state.salaryChange.push(action.payload)
        };
        break;
      case 'DECREASE':
        state = {
          ...state,
          salary: state.salary -= action.payload,
          salaryChange: state.salaryChange.push(action.payload)
        };
        break;
      default:
    }
    return state;
  };

  store = createStore(this.reducer)

  render() {
    return (
        <div>
          <h6>Learn Integrate React with Redux</h6>
          See result in console.
        </div>
    );
  }
}

export default IntegrateReactRedux;
