import React, {Component} from 'react';
import {createStore, combineReducers, applyMiddleware} from 'redux';

class LearnReduxMiddleware extends Component {
  componentDidMount() {
    this.subscribeState(this.store);
    this.changeState(this.store, this.addSalaryBy(15000));
    this.changeState(this.store, this.subtractSalaryBy(8000));
    this.changeState(this.store, this.changeNameBy('Sila'));
    this.changeState(this.store, this.changeAgeBy(20))
  }

  initEmployeeState = {name: 'Korn', age: 18};
  initSalaryState = {salary: 15000, salaryChange: []};

  employeeReducer = (state = this.initEmployeeState, action) => {
    switch (action.type) {
      case 'SET_NAME':
        state = {
          ...state,
          name: action.name
        };
        break;
      case 'SET_AGE':
        state = {
          ...state,
          age: action.age
        };
        break;
      default:
        return state;
    }
    return state
  };

  salaryReducer = (state = this.initSalaryState, action) => {
    switch (action.type) {
      case 'ADD':
        return state = {
          ...state,
          salary: state.salary += action.payload,
          salaryChange: [...state.salaryChange, action.payload]
        };
      case 'SUBTRACT':
        return state = {
          ...state,
          salary: state.salary -= action.payload,
          salaryChange: [...state.salaryChange, action.payload]
        };
      default:
        return state;
    }
  };

  logger = store => next => action => {
    console.log('Log Action', action);
    next(action)
  };

  store = createStore(combineReducers({salaryReducer: this.salaryReducer, employeeReducer: this.employeeReducer}), {}, applyMiddleware(this.logger));

  subscribeState = store => store.subscribe(() => console.log('State is Updated', store.getState()));

  addSalaryBy = (salary) => ({type: 'ADD', payload: salary});
  subtractSalaryBy = (salary) => ({type: 'SUBTRACT', payload: salary});
  changeNameBy = (name) => ({type: 'SET_NAME', name});
  changeAgeBy = (age) => ({type: 'SET_AGE', age});

  changeState = (store, action) => {
    store.dispatch(action);
  };

  render() {
    return (
        <div>
          <h6>Learn Middleware</h6>
          <p>Middleware is a observer to observe Action before send to Reducer.</p>
          See result in console.
        </div>
    );
  }
}

export default LearnReduxMiddleware;
