import React from 'react';
import {createStore} from 'redux'

export default class LearnMultiState extends React.Component {
  componentDidMount() {
    this.subscribeState(this.store);
    this.changeState(this.store, this.addBy(15000));
    this.changeState(this.store, this.addBy(15000));
    this.changeState(this.store, this.addBy(30000));
    this.changeState(this.store, this.subtractBy(8000));
  }

  initialState = {
    salary: 15000,
    salaryChange: []
  };

  reducer = (state = this.initialState, action) => {
    switch (action.type) {
      case 'ADD':
        return state = {
          ...state,
          salary: state.salary += action.payload,
          salaryChange: [...state.salaryChange, action.payload]
        };
      case 'SUBTRACT':
        return state = {
          ...state,
          salary: state.salary -= action.payload,
          salaryChange: [...state.salaryChange, action.payload]
        };
      default:
        return state;
    }
  };

  store = createStore(this.reducer);

  subscribeState = (store) => store.subscribe(() => console.log('StateUpdated', store.getState()));

  addBy = salary => ({type: 'ADD', payload: salary});
  subtractBy = salary => ({type: 'SUBTRACT', payload: salary});

  changeState = (store, action) => {
    store.dispatch(action);
  };

  render() {
    return (
        <div>
          <h6>Learn Multi State</h6>
          See result in console.
        </div>
    )
  }
}
