import React from 'react';
import {Menu, Typography} from 'antd'
import {Link, Route, Switch, useRouteMatch} from 'react-router-dom'

import ReduxFunda from './learn-redux/ReduxFunda';
import LearnMultiState from './learn-redux/LearnMultiState.js';
import LearnMultiReducer from './learn-redux/LearnMultiReducer';
import LearnReduxMiddleware from './learn-redux/LearnReduxMiddleware';
import IntegrateReactRedux from './learn-redux/IntegrateReactRedux';

const {Title} = Typography;

export default function Sandbox() {
  const match = (useRouteMatch('/sandbox'));
  return (
      <div>
        <div className="container-fluid">
          <Title level={3} style={{margin: 0}}>Sandbox: My Learning about React, Redux </Title>
          <Title level={4} style={{marginTop: 0}} type="secondary">Redux Fundamental: </Title>
          <Menu mode="horizontal">
            <Menu.Item>
              <Link to={`${match.url}/redux-fundamental`}>ReduxFunda</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/learn-multiple-state`}>Learn Multiple States</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/learn-multiple-reducer`}>Learn Multiple Reducers</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/learn-redux-middleware`}>Learn Redux Middleware</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/integrate-react-with-redux`}>Learn Integrate React with Redux</Link>
            </Menu.Item>
          </Menu>
          <Switch>
            <Route path={`${match.path}/redux-fundamental`}><ReduxFunda/></Route>
            <Route path={`${match.path}/learn-multiple-state`}><LearnMultiState/></Route>
            <Route path={`${match.path}/learn-multiple-reducer`}><LearnMultiReducer/></Route>
            <Route path={`${match.path}/learn-redux-middleware`}><LearnReduxMiddleware/></Route>
            <Route path={`${match.path}/integrate-react-with-redux`}><IntegrateReactRedux/></Route>
          </Switch>
        </div>
      </div>
  )
}
