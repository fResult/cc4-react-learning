// 1-1
let arr1 = [1, 2, 30, 400];
let arr2 = arr1.map(x => x * 2)
console.log(arr2)

// 1-2
array1 = [1, 2, 3, 4];
array2 = array1.map(x => x.toString());
console.log(array2);

// 1-3
array1 = [1, "1", 2, {}];
array2 = array1.map(x => typeof x);
console.log(array2);

// 1-4
array1 = ["apple", "banana", "orange"];
array2 = array1.map(x => x.toUpperCase());
console.log(array2);

// 1-5
array1 = [
  {name: "apple", age: 14},
  {name: "banana", age: 18},
  {name: "watermelon", age: 32},
];
array2 = array1.map(x => x.name);
console.log(array2);

// 1-6
array1 = [
  {name: "apple", age: 14},
  {name: "banana", age: 18},
  {name: "watermelon", age: 32},
];
array2 = array1.map(x => x.age);
console.log(array2);

// 1-7
array1 = [
  {name: "apple", surname: "London"},
  {name: "banana", surname: "Bangkok"},
  {name: "watermelon", surname: "Singapore"},
];
array2 = array1.map(x => `${x.name} ${x.surname}`);
console.log(array2);

// 1-8
array1 = [1, 3, 4, 5, 6, 7, 8];
array2 = array1.map(x => x % 2 === 0 ? 'even' : 'odd');
console.log(array2);

// 1-9
array1 = [1, -3, 2, 8, -4, 5];
array2 = array1.map(x => Math.abs(x));
console.log(array2);

// 1-10
array1 = [100, 200.25, 300.84, 400.3];
array2 = array1.map(x => x.toFixed(2));
console.log(array2);


// 1-11
array1 = [
  {name: "apple", birth: "2000-01-01"},
  {name: "banana", birth: "1990-10-01"},
  {name: "watermelon", birth: "1985-12-01"},
];
array2 = array1.map(x => {
  return {
    name: x.name,
    birth: x.birth,
    age: Math.floor((Date.now() - new Date(x.birth)) / (31557600000))
  }
});
console.log(array2);

// 1-12
array1 = [
  {name: "apple", birth: "2000-01-01"},
  {name: "banana", birth: "1990-10-10"},
  {name: "watermelon", birth: "1985-12-30"},
];
array2 = array1.map(x => `
<tr>
  <td>${x.name}</td> 
  <td>${new Date(x.birth).toString().split(' ').filter((part, idx) => idx <= 2 ? part : '').toString().split(',').join(' ')}</td>
</tr>
`);
console.log(array2);
{/*<!--  <td>${new Date(x.birth).getDate()}</td>-->*/
}
{/*<!--  <td>${new Date(x.birth).toString().split(' ').filter((part, idx) => idx <= 2 ? part : '').toString().split(',').join(' ')}</td> -->*/
}
