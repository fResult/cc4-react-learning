const array1 = [
      { name: "apple", surname: "London" },
      { name: "banana", surname: "Bangkok" },
      { name: "watermelon", surname: "Singapore" },
    ];

const array2 = array1.map(x=> `${x.name} ${x.surname}`);

console.log(array2);
