const array1 = [
  {name: "apple", birth: "2000-01-01"},
  {name: "banana", birth: "1990-10-10"},
  {name: "watermelon", birth: "1985-12-30"},
];

const array2 = array1.map(x => `
<tr>
  <td>${x.name}</td> 
  <td>${new Date(x.birth).toString().split(' ').filter((part, idx) => idx <= 2 ? part : '').toString().split(',').join(' ')}</td>
</tr>
`);

console.log(array2);
