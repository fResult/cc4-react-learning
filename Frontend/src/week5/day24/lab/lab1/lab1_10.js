const array1 = [100, 200.25, 300.84, 400.3];

const array2 = array1.map(x => x.toFixed(2));

console.log(array2);
