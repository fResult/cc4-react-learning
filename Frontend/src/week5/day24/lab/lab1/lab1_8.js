const array1 = [1, 3, 4, 5, 6, 7, 8];

const array2 = array1.map(x => x % 2 === 0 ? 'even' : 'odd');

console.log(array2);
