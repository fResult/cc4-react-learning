const array1 = [
  {name: "apple", birth: "2000-01-01"},
  {name: "banana", birth: "1990-10-01"},
  {name: "watermelon", birth: "1985-12-01"},
];

const array2 = array1.map(x => {
  return {
    name: x.name,
    birth: x.birth,
    age: Math.floor((Date.now() - new Date(x.birth)) / (31557600000))
  }
});

console.log(array2);
