// 2-1
let array1 = [1, 2, 30, 400];
let array2 = array1.filter(x => x > 10);
console.log('2-1');
console.log(array2);

// 2-2
console.log('\n2-2');
array1 = [1, 2, 3, 4];
array2 = array1.filter(x => x % 2 === 1);
console.log(array2);

// 2-3
console.log('\n2-3');
array1 = [1, "1", 2, {}];
array2 = array1.filter(x => Number.isInteger(x));
console.log(array2);

// 2-4
console.log('\n2-4');
array1 = ["apple", "banana", "orange", "pineapple", "watermelon"];
array2 = array1.filter(x => x.length > 6);
console.log(array2);

// 2-5
console.log('\n2-5');
array1 = [
  { name: "apple", age: 14 },
  { name: "banana", age: 18 },
  { name: "watermelon", age: 32 },
  { name: "pineapple", age: 16 },
  { name: "peach", age: 24 },
];
array2 = array1.filter(x => x.age < 18);
console.log(array2);

// 2-6
console.log('\n2-6');
array1 = [
  { name: "apple", age: 14 },
  { name: "banana", age: 18 },
  { name: "watermelon", age: 32 },
  { name: "pineapple", age: 16 },
  { name: "peach", age: 24 },
];
array2 = array1.filter(x => x.age !== 32);
console.log(array2);

// 2-7
console.log('\n2-7');
array1 = [1, -3, 2, 8, -4, 5];
array2 = array1.filter(x => x > 0);
console.log(array2);

// 2-8
console.log('\n2-8');
array1 = [1,3,4,5,6,7,8];
array2 = array1.filter(x => x % 3 === 0);
console.log(array2);

// 2-9
console.log('\n2-9');
array1 = ["peach", 1, -3, "2", {}, []];
array2 = array1.filter(x => typeof x === 'string');
console.log(array2);

// 2-10
console.log('\n2-10');
array1 = ["APPLE", "appLE", "PEACH", "PEach"]
array2 = array1.filter(x => x.toUpperCase() === x)
console.log(array2);

// 2-11
console.log('\n2-11');
array1 = [
  { name: "apple", birth: "2001-01-01" },
  { name: "banana", birth: "1990-10-10" },
  { name: "watermelon", birth: "1985-12-30" },
  { name: "peach", birth: "2002-10-13" },
];
array2 = array1.filter(x => new Date(x.birth).getMonth() === 9);
console.log(array2);

// 2-12
console.log('\n2-12');
array1 = [
  { name: "apple", birth: "2001-01-01" },
  { name: "banana", birth: "1990-10-10" },
  { name: "watermelon", birth: "1985-12-30" },
  { name: "peach", birth: "2002-10-13" },
];
array2 = array1.filter(x => new Date(x.birth).getFullYear() < 2000 );
console.log(array2);
