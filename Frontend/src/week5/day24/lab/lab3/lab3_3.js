import React, {Component} from 'react';

class Lab3_3 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      people: [
        {
          firstName: "Gourge",
          lastName: "Longman",
          text: 'Some text 1',
          image: 'https://image.freepik.com/free-photo/white-horses-camargue-france_119101-9.jpg'
        },
        {
          firstName: "Zofia",
          lastName: "Olson",
          text: 'Some text 2',
          image: 'https://image.freepik.com/free-photo/tiger-looking-straight-ahead_1286-73.jpg?1'
        },
        {
          firstName: "Elif",
          lastName: "Salt",
          text: 'Some text 3',
          image: 'https://image.freepik.com/free-photo/3d-landscape-with-herd-elephants_1048-7804.jpg'
        },
        {
          firstName: "Kyal",
          lastName: "Hogan",
          text: 'Some text 4',
          image: 'https://image.freepik.com/free-photo/cute-pug-with-santa-hat-gift-laying_23-2148348108.jpg'
        },
      ]
    }

  }

  render() {
    return (
        <div>
          <h6>Lab 3-3: Render Post</h6>
          <div className="container-fluid">
            <div className="row">
              <div className="col-3"/>

              <div className="col-6">{
                this.state.people.map((person, idx) => {
                  return (
                      <div key={idx} className="card" style={{marginBottom: '30px'}}>
                        <div className="card-img-top">
                          <img style={{width: '100%'}} src={person.image} alt="Just Alt"/>
                        </div>
                        <div className="card-body">
                          <p>{`${person.firstName} ${person.lastName}`}</p>
                          <p>{person.text}</p>
                        </div>
                      </div>
                  )
                })
              }</div>

              <div className="col-3"/>
            </div>
          </div>
        </div>
    )
  }
}

export default Lab3_3;
