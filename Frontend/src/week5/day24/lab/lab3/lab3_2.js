import React, {Component} from 'react';

class Lab3_2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      people: [
        {firstName: 'Gourge', lastName: 'Longman', age: 0},
        {firstName: 'Zofia', lastName: 'Olson', age: 0},
        {firstName: 'Elif', lastName: 'Salt', age: 0},
        {firstName: 'Kyal', lastName: 'Hogan', age: 0}
      ]
    }
  }

  render() {
    return (
        <div>
          <h6>Lab 3-2: Render Table</h6>
          <table>
            <tbody>{
              this.state.people.map((person, idx) => {
                return (
                    <tr key={idx}>
                      <td>{person.firstName}</td>
                      <td>{person.lastName}</td>
                      <td>{person.age}</td>
                    </tr>
                )
              })
            }</tbody>
          </table>
        </div>
    );
  }
}

export default Lab3_2;
