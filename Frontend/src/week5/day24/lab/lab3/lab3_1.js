import React, {Component} from 'react';

class Lab3_1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alphabet: [
        {name: 'a'},
        {name: 'b'},
        {name: 'c'},
        {name: 'd'},
        {name: 'e'},
        {name: 'f'},
        {name: 'g'}
      ]
    }
  }

  render() {
    return (
        <div>
          <h6>Lab 3-1: Alphabet a - h</h6>
          <ul>
            {this.state.alphabet.map(char => <li key={char.name}>{char.name}</li>)}
          </ul>
        </div>
    );
  }
}

export default Lab3_1;
