import React, {Component} from 'react';
// import {
//   BrowserRouter as Router,
//   Switch,
//   Route,
//   NavLink,
//   Link
// } from 'react-router-dom'
// import SmartPhone from "../product/smart-phone/SmartPhone";
// import Computer from "../product/computer/Computer";
// import Gadget from "../product/gadget/Gadget";

class ProductCategoryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const {onShowProducts} = this.props
    return (
        <div>
          Product Category List
          <ul>
            <li><a href={"#/smart-phone"} onClick={() => onShowProducts('SmartPhone')}>Smart Phone</a></li>
            <li><a href={"#/computer"} onClick={() => onShowProducts('Computer')}>Computer</a></li>
            <li><a href={"#/gadget"} onClick={() => onShowProducts('Gadget')}>Gadget</a></li>
          </ul>
          {/*<Router>*/}
          {/*  <div className="nav flex-column">*/}
          {/*    <Link to="/lab3_4-day24/smart-phone">Smart Phone</Link>*/}
          {/*    <Link to="/lab3_4-day24/computer">Computer</Link>*/}
          {/*    <Link to="/lab3_4-day24/gadget">Gadget</Link>*/}
          {/*  </div>*/}
          {/*</Router>*/}
        </div>
    );
  }
}

export default ProductCategoryList;
