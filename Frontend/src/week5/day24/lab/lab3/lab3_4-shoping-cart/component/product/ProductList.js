import React, {Component} from 'react';
// import {
//   BrowserRouter as Router,
//   Switch,
//   // Route,
//   // Link
// } from 'react-router-dom'

import styles from './ProductList.module.css';
// {/*import Computer from "./computer/Computer";*/}
// import SmartPhone from "./smart-phone/SmartPhone";
// import Gadget from "./gadget/Gadget";

class ProductList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      products: []
      // products: [
      //   {
      //     id: 1574412046612,
      //     name: 'Nokia Lumia',
      //     desc: 'พาร์ โซน คันถธุระวิปรองรับพิซซ่า ดั๊มพ์ แฮนด์สปอต เลกเชอร์บูติคกฤษณ์ระโงกซีนีเพล็กซ์ บ็อกซ์ฟลอร์ แชมปิยองพาวเวอร์แอ๊บแบ๊วฟาสต์ฟู้ด โค้ก คำตอบเคลม แอ๊บแบ๊วว้อยวอลซ์ โค้ชตัวตนแดนซ์สต๊อค พันธกิจเซ็นเซอร์โมเต็ล ชาร์ตเอาท์ดอร์ เปเปอร์เซ็กซี่วอเตอร์ยังไง ไทยแลนด์',
      //     image: 'https://images-na.ssl-images-amazon.com/images/I/413pD2TNh8L.jpg'
      //   },
      //   {
      //     id: 1574412085741,
      //     name: 'Huawei P5',
      //     desc: 'เอ็กซ์โปธุรกรรมช็อปเที่ยงวัน บลูเบอร์รี่ไฟลต์คอมเมนต์ชะโนด แผดเผาชนะเลิศเอ็นเตอร์เทนเปเปอร์ เกรย์ เรซินเท็กซ์ติงต๊อง ทำงาน แอปเปิ้ลแหววมัฟฟินรีสอร์ทเซรามิก สเตชันสปอร์ตแชมพู เซลส์ธุรกรรมพุทธภูมิโคโยตี้โหลน สติกเกอร์สังโฆม้งเสกสรรค์ เอ๊าะกรีนฟิวเจอร์ แทคติคเอาท์ดอร์เซนเซอร์ เป่ายิ้งฉุบบอยคอตต์เซลส์แมน ไฮเทคนิวโอเปอเรเตอร์ซีอีโอโนติส ซามูไรหน่อมแน้มลาตินแมมโบ้ ไลน์แฮมเบอร์เกอร์',
      //     image: 'https://consumer-img.huawei.com/content/dam/huawei-cbg-site/common/mkt/pdp/phones/y5-prime-2018/img/pic_s11-quality-image-original-v2.png'
      //   },
      //   {
      //     id: 1574412096513,
      //     name: 'iPhone XI',
      //     desc: 'วานิลลาคอร์รัปชั่นชัวร์เต๊ะกระดี๊กระด๊า ทัวร์ สไลด์วิดีโอเซ็กซี่ เพนกวินสปายเป่ายิ้งฉุบทาวน์เฮาส์โหลยโท่ย ดิสเครดิตแอสเตอร์ ยอมรับแอคทีฟแพนงเชิญ อิเหนาฮิปฮอปแฟรีลิมิตบรรพชน สตีล ไรเฟิล ฟลอร์แฟ็กซ์ แทคติคโรแมนติคโบว์เดโม ไวอะกร้าแอนด์ วอเตอร์กราวนด์คาแรคเตอร์ ชัวร์บาบูนวโรกาสมหภาคอพาร์ทเมนต์ เวิร์คตังค์คอรัปชัน คอร์ปอเรชั่นวิลล์แลนด์สปอร์ตระโงก',
      //     image: 'https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-11-pro-max-gold-select-2019?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1566953859132'
      //   }
      // ]
    }
  }

  render() {
    return (
        <div className={styles.Root}>
          Product List
          <div key={this.id} className={`container ${styles.ProductsContainer}`}>{
            this.props.products.map(product => {
              return (
                  <div key={product.id}>
                    <div className={`card ${styles.ProductContainer}`}>
                      <div className={`card-header ${styles.ProductHeader}`}>{product.name}</div>
                      <div className="container">
                        <div className="row">
                          <div className="col">
                            <div className="card-img">
                              <img src={product.image} style={{width: '50%', height: '50%', maxHeight: 300}} alt="Just Alt"/>
                            </div>
                          </div>
                          <div className="col">
                            {product.desc}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              )
            })
          }</div>
        </div>
    );
  }
}

// {/*<Router>*/
// }
// {/*  <Switch>*/
// }
// // </Switch>
// // </Router>
// {/*<Route path="/lab3_4-day24/smart-phone" component={SmartPhone}><SmartPhone/></Route>*/
// }
// {/*<Route path="/lab3_4-day24/computer" component={Computer}></Route>*/
// }
// {/*<Route path="/lab3_4-day24/gadget" component={Gadget}/>*/
// }

export default ProductList;
