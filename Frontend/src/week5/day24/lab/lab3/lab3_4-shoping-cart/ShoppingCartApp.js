import React, {Component} from 'react';
import ProductCategoryList from "./component/category/ProductCategoryList";
import ProductList from "./component/product/ProductList";
import Cart from "./component/cart/Cart";

class ShoppingCartApp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      products: {
        smartPhone: [
          {
            id: 1574412046612,
            name: 'Nokia Lumia',
            desc: 'พาร์ โซน คันถธุระวิปรองรับพิซซ่า ดั๊มพ์ แฮนด์สปอต เลกเชอร์บูติคกฤษณ์ระโงกซีนีเพล็กซ์ บ็อกซ์ฟลอร์ แชมปิยองพาวเวอร์แอ๊บแบ๊วฟาสต์ฟู้ด โค้ก คำตอบเคลม แอ๊บแบ๊วว้อยวอลซ์ โค้ชตัวตนแดนซ์สต๊อค พันธกิจเซ็นเซอร์โมเต็ล ชาร์ตเอาท์ดอร์ เปเปอร์เซ็กซี่วอเตอร์ยังไง ไทยแลนด์',
            image: 'https://images-na.ssl-images-amazon.com/images/I/413pD2TNh8L.jpg'
          },
          {
            id: 1574412085741,
            name: 'Huawei P5',
            desc: 'เอ็กซ์โปธุรกรรมช็อปเที่ยงวัน บลูเบอร์รี่ไฟลต์คอมเมนต์ชะโนด แผดเผาชนะเลิศเอ็นเตอร์เทนเปเปอร์ เกรย์ เรซินเท็กซ์ติงต๊อง ทำงาน แอปเปิ้ลแหววมัฟฟินรีสอร์ทเซรามิก สเตชันสปอร์ตแชมพู เซลส์ธุรกรรมพุทธภูมิโคโยตี้โหลน สติกเกอร์สังโฆม้งเสกสรรค์ เอ๊าะกรีนฟิวเจอร์ แทคติคเอาท์ดอร์เซนเซอร์ เป่ายิ้งฉุบบอยคอตต์เซลส์แมน ไฮเทคนิวโอเปอเรเตอร์ซีอีโอโนติส ซามูไรหน่อมแน้มลาตินแมมโบ้ ไลน์แฮมเบอร์เกอร์',
            image: 'https://consumer-img.huawei.com/content/dam/huawei-cbg-site/common/mkt/pdp/phones/y5-prime-2018/img/pic_s11-quality-image-original-v2.png'
          },
          {
            id: 1574412096513,
            name: 'iPhone XI',
            desc: 'วานิลลาคอร์รัปชั่นชัวร์เต๊ะกระดี๊กระด๊า ทัวร์ สไลด์วิดีโอเซ็กซี่ เพนกวินสปายเป่ายิ้งฉุบทาวน์เฮาส์โหลยโท่ย ดิสเครดิตแอสเตอร์ ยอมรับแอคทีฟแพนงเชิญ อิเหนาฮิปฮอปแฟรีลิมิตบรรพชน สตีล ไรเฟิล ฟลอร์แฟ็กซ์ แทคติคโรแมนติคโบว์เดโม ไวอะกร้าแอนด์ วอเตอร์กราวนด์คาแรคเตอร์ ชัวร์บาบูนวโรกาสมหภาคอพาร์ทเมนต์ เวิร์คตังค์คอรัปชัน คอร์ปอเรชั่นวิลล์แลนด์สปอร์ตระโงก',
            image: 'https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-11-pro-max-gold-select-2019?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1566953859132'
          }
        ],
        computer: [
          {
            id: 1574415089532,
            name: 'Lenovo',
            desc: 'เอ็กซ์โปธุรกรรมช็อปเที่ยงวัน บลูเบอร์รี่ไฟลต์คอมเมนต์ชะโนด แผดเผาชนะเลิศเอ็นเตอร์เทนเปเปอร์ เกรย์ เรซินเท็กซ์ติงต๊อง ทำงาน แอปเปิ้ลแหววมัฟฟินรีสอร์ทเซรามิก สเตชันสปอร์ตแชมพู เซลส์ธุรกรรมพุทธภูมิโคโยตี้โหลน สติกเกอร์สังโฆม้งเสกสรรค์ เอ๊าะกรีนฟิวเจอร์ แทคติคเอาท์ดอร์เซนเซอร์ เป่ายิ้งฉุบบอยคอตต์เซลส์แมน ไฮเทคนิวโอเปอเรเตอร์ซีอีโอโนติส ซามูไรหน่อมแน้มลาตินแมมโบ้ ไลน์แฮมเบอร์เกอร์',
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQoAQwrGTw4e2zR54MxPE75VpVTxTHMsJCtiyvzk3mOyAPomF2_A&s'
          },
          {
            id: 1574415118766,
            name: 'Asus',
            desc: 'วานิลลาคอร์รัปชั่นชัวร์เต๊ะกระดี๊กระด๊า ทัวร์ สไลด์วิดีโอเซ็กซี่ เพนกวินสปายเป่ายิ้งฉุบทาวน์เฮาส์โหลยโท่ย ดิสเครดิตแอสเตอร์ ยอมรับแอคทีฟแพนงเชิญ อิเหนาฮิปฮอปแฟรีลิมิตบรรพชน สตีล ไรเฟิล ฟลอร์แฟ็กซ์ แทคติคโรแมนติคโบว์เดโม ไวอะกร้าแอนด์ วอเตอร์กราวนด์คาแรคเตอร์ ชัวร์บาบูนวโรกาสมหภาคอพาร์ทเมนต์ เวิร์คตังค์คอรัปชัน คอร์ปอเรชั่นวิลล์แลนด์สปอร์ตระโงก',
            image: 'http://www.gadgetdetail.com/wp-content/uploads/2016/04/asus_rog_g752vy.jpg'
          },
          {
            id: 1574415133033,
            name: 'Macbook',
            desc: 'พาร์ โซน คันถธุระวิปรองรับพิซซ่า ดั๊มพ์ แฮนด์สปอต เลกเชอร์บูติคกฤษณ์ระโงกซีนีเพล็กซ์ บ็อกซ์ฟลอร์ แชมปิยองพาวเวอร์แอ๊บแบ๊วฟาสต์ฟู้ด โค้ก คำตอบเคลม แอ๊บแบ๊วว้อยวอลซ์ โค้ชตัวตนแดนซ์สต๊อค พันธกิจเซ็นเซอร์โมเต็ล ชาร์ตเอาท์ดอร์ เปเปอร์เซ็กซี่วอเตอร์ยังไง ไทยแลนด์',
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQyVAyw-DaE84EI6dzcgP_8YCFHkNyneJ2kwoUHLcnneueN4Kq2&s'
          },
        ],
        gadget: [
          {
            id: 1574418893850,
            name: 'Smart Phone Case',
            desc: 'วานิลลาคอร์รัปชั่นชัวร์เต๊ะกระดี๊กระด๊า ทัวร์ สไลด์วิดีโอเซ็กซี่ เพนกวินสปายเป่ายิ้งฉุบทาวน์เฮาส์โหลยโท่ย ดิสเครดิตแอสเตอร์ ยอมรับแอคทีฟแพนงเชิญ อิเหนาฮิปฮอปแฟรีลิมิตบรรพชน สตีล ไรเฟิล ฟลอร์แฟ็กซ์ แทคติคโรแมนติคโบว์เดโม ไวอะกร้าแอนด์ วอเตอร์กราวนด์คาแรคเตอร์ ชัวร์บาบูนวโรกาสมหภาคอพาร์ทเมนต์ เวิร์คตังค์คอรัปชัน คอร์ปอเรชั่นวิลล์แลนด์สปอร์ตระโงก',
            image: 'https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/MX022?wid=1144&hei=1144&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1566858426321'
          },
          {
            id: 1574418911265,
            name: 'Smart Phone Camera Lens',
            desc: 'พาร์ โซน คันถธุระวิปรองรับพิซซ่า ดั๊มพ์ แฮนด์สปอต เลกเชอร์บูติคกฤษณ์ระโงกซีนีเพล็กซ์ บ็อกซ์ฟลอร์ แชมปิยองพาวเวอร์แอ๊บแบ๊วฟาสต์ฟู้ด โค้ก คำตอบเคลม แอ๊บแบ๊วว้อยวอลซ์ โค้ชตัวตนแดนซ์สต๊อค พันธกิจเซ็นเซอร์โมเต็ล ชาร์ตเอาท์ดอร์ เปเปอร์เซ็กซี่วอเตอร์ยังไง ไทยแลนด์',
            image: 'https://img10.jd.co.th/n0/jfs/t7/215/726754591/218655/2d70ecdd/5d1329c2N1c1caf4d.jpeg!q70.jpg'
          }
        ]
      }
    }
  }

  products = [];

  showSmartPhone(products) {
    this.products = products.smartPhone;
    this.setState({products})
  }

  showComputer(products) {
    this.products = products.computer;
    this.setState({products})
  }

  showGadget(products) {
    this.products = products.gadget;
    this.setState({products})
  }

  handleShowProducts = (whatProduct) => {
    // switch (whatProduct) {
    //   case 'SmartPhone':
    //     return this.showSmartPhone();
    //   case 'Computer':
    //     return this.showComputer();
    //   case 'Gadget':
    //     return this.showGadget();
    switch (whatProduct) {
      case 'SmartPhone':
        this.showSmartPhone(this.state.products); break;
      case 'Computer':
        this.showComputer(this.state.products); break;
      case 'Gadget':

        this.showGadget(this.state.products); break;
      default: return
    }
  }

  render() {
    return (
        <div>
          <h6>Lab 3-4: Shopping Cart Application</h6>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm-12 col-md-2">
                <ProductCategoryList onShowProducts={this.handleShowProducts}/>
              </div>
              <div className="col-sm-12 col-md-7">
                <ProductList products={this.products}/>
              </div>
              <div className="col-sm-12 col-md-3">
                <Cart/>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default ShoppingCartApp;
