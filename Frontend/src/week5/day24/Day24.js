import React from 'react';
import {Menu, Typography} from 'antd'
import {
  Switch,
  Route,
  useRouteMatch,
  Link
} from 'react-router-dom'

import Lab3Part1 from "./lab/lab3/lab3_1";
import Lab3Part2 from "./lab/lab3/lab3_2";
import Lab3Part3 from "./lab/lab3/lab3_3";
import ShoppingCartApp from "./lab/lab3/lab3_4-shoping-cart/ShoppingCartApp";

const {Title} = Typography;

function Day24(props) {
  const match = useRouteMatch('/week5/day24');
  return (
      <div>
        <Title level={4}>Day24: {new Date('2019-11-22').toDateString()}</Title>
        <Menu mode="horizontal">
          <Menu.Item>
            <Link to={`${match.url}/lab3_1-day24`}>Lab 3_1</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`${match.url}/lab3_2-day24`}>Lab 3_2</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`${match.url}/lab3_3-day24`}>Lab 3_3</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`${match.url}/shopping-cart`}>Lab 3_4: Shopping Cart Application</Link>
          </Menu.Item>
        </Menu>

        <Switch>
          <Route path={`${match.path}/lab3_1-day24`}><Lab3Part1/></Route>
          <Route path={`${match.path}/lab3_2-day24`}><Lab3Part2/></Route>
          <Route path={`${match.path}/lab3_3-day24`}><Lab3Part3/></Route>
          <Route path={`${match.path}/shopping-cart`}><ShoppingCartApp/></Route>
        </Switch>
      </div>
  )
}

export default Day24;
