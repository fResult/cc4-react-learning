import React, {Component} from 'react'

const initialState = {count: 0, show: true};

function counterReducer(state, action) {
  console.log(state, action)
  switch (action.type) {
    case 'INCREMENT':
      return {...state, count: state.count + 1};
    case 'DECREMENT':
      return {...state, count: state.count - 1};
    case 'TOGGLE':
      return {...state, show: !state.show}
    default:
      return state
  }
}

const increment = () => ({type: 'INCREMENT'});
const decrement = () => ({type: 'DECREMENT'});
const toggle = () => ({type: 'TOGGLE'});

const {createStore} = require('../../day22/lab-redux/lab3-create-store.js');

let store = createStore(counterReducer, initialState)

export default class Lab1 extends Component {
  state = {count: 0};

  componentDidMount = () => store.subscribe(() => this.forceUpdate());

  handleAdd = () => store.dispatch(increment());
  handleSubtract = () => store.dispatch(decrement());
  handleToggle = () => store.dispatch(toggle());

  render() {
    const {count, show} = store.getState();
    return (
        <div>
          {
            show && (
                <div>counter: {count}</div>
            )
          }
          <button onClick={this.handleAdd}>Add</button>
          <button onClick={this.handleSubtract}>Subtract</button>
          <button onClick={this.handleToggle}>Show/Hide</button>
        </div>
    )
  }
}
