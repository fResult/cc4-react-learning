import React from 'react';
import Lab1 from "./lab-redux/lab1-redux-with-counter-app";


function Day23() {
  return (
    <div>
      <h5>Day 23: {new Date('2019-11-21').toDateString()}</h5>
      <Lab1/>
    </div>
  )
}

export default Day23;
