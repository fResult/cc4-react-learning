const g = () => console.log('do nothing');

console.log(g());

const doSomething = callback => {
  callback()
  return 1
};

console.log(doSomething(g));

const super_add = x => y => x + y

console.log(super_add(1)(2));


const super_exponent = x => y => y ** x
const exponentBy3 = super_exponent(3)
console.log(super_exponent(3)(4))
console.log(exponentBy3(4));
