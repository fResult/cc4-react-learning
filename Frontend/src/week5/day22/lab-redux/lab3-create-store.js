import React from 'react';

export function createStore(reducer, initialState) {
  let state = initialState;
  let subscribers = [];

  return {
    getState: () => state,
    dispatch: (action) => {
      state = reducer(state, action);
      subscribers.forEach(callback => callback(state))
    },
    subscribe: callback => {
      subscribers.push(callback);
      return subscribers.length - 1;
    },
    unsubscribe: id => subscribers[id] = null,
  }
}

function incrReducer(state = {num: 0}, action) {
  switch (action.type) {
    case 'INCREMENT':
      return {...state, num: state.num + action.num};
    case 'DECREMENT':
      return {...state, num: state.num - action.num};
    default:
      return state
  }
}

function incrementBy(num) {
  return {type: 'INCREMENT', num: num}
}

export default function Lab3Redux(props) {
  const incrStore = createStore(incrReducer, {num: 0});
  incrStore.subscribe(() => console.log(`The ${JSON.stringify(incrStore.getState())} is updated`));

  incrStore.dispatch(incrementBy(2));
  incrStore.dispatch(incrementBy(3));
  incrStore.dispatch(incrementBy(-5));
  incrStore.dispatch(incrementBy(-7));
  incrStore.dispatch(incrementBy(12));

  return (
      <div>
        <h6>Lab 3: Redux: Create Store</h6>
        See result in console.
      </div>
  )
}


