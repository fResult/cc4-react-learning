import React from 'react';
import Lab3Redux from "./lab-redux/lab3-create-store";


function Day22() {
  return (
      <div>
        <h5>Day 22: {new Date('2019-11-20').toDateString()}</h5>
        <Lab3Redux/>
      </div>
  )
}

export default Day22;
