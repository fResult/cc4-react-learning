import React, {Component} from 'react';

import styles from './TodoList.module.css';

class TodoList extends Component {
  render() {
    const {todoList, onDeleteTodo, onCompleteTodo} = this.props;

    const completeTodo = (todoId) => {
      onCompleteTodo(todoId)
    };

    return (
        <div>
          <ul style={{listStyleType: 'none'}}>{
            todoList.map(todo => (
                <li key={todo.id}>
                  <div className="container-fluid">
                    <div className="row">
                      <div className="col-auto">
                        <input type="checkbox" onInput={() => completeTodo(todo.id)}/>
                      </div>
                      <div className="col">
                        <span className={styles.TodoText} style={todo.isCompleted === true ? {textDecoration: 'line-through'} : null}>{todo.text}</span>
                      </div>
                      <div className="col-auto">
                        <button onClick={() => onDeleteTodo(todo.id)}>X</button>
                      </div>
                    </div>
                  </div>
                </li>
            ))
          }</ul>
        </div>
    );
  }
}

export default TodoList;
