import React from 'react';

import styles from './NewTodo.module.css';

export default function NewTodo(props) {
  return (
      <div>
        <div className="container-fluid">
          <div className="row">
            <div className="col">
              <input className={`${styles.TodoInput}`} placeholder="Enter Todo"/>
            </div>
            <div className="col-auto">
              <button className="btn"
                      onClick={(e) => {props.onAddTodo(e, document.querySelector(`.${styles.TodoInput}`).value)}}
              >Add</button>
            </div>
          </div>
        </div>
      </div>
  );
}

