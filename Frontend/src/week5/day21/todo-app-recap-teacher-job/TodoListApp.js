import React, {Component} from 'react';

// import styles from './TodoListApp.module.css';

import NewTodo from "./components/NewTodo";
import TodoList from "./components/TodoList";

class TodoListApp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      todoList: [
        {id: 1574147432423, text: 'Play football', isCompleted: false},
        {id: 1574147450742, text: 'Do Yoga', isCompleted: false},
        {id: 1574147480360, text: 'Have Dinner', isCompleted: false},
      ]
    }
  }

  handleAddTodo = (e, todoText) => {
    // e.preventDefault();
    let todoList = [...this.state.todoList, {id: Date.now(), text: todoText, isComplete: false}];
    this.setState({todoList: todoList})
  };

  handleDeleteTodo = todoId => {
    this.setState({todoList: this.state.todoList.filter(todo => todo.id !== todoId)});
  };

  handleCompleteTodo = todoId => this.setState(state => {
    state.todoList.map(todo => (todo.id === todoId) ? todo.isCompleted = !todo.isCompleted : '')
  });

  render() {
    return (
        <div className="container">
          <div className="row">
            <div className="col-sm-2 col-md-3"/>

            <div className="col-sm-8 col-md-6">
              <div className="card">
                <div className='card-header'>
                  Todo List Application
                </div>
                <div className="card-body">
                  <NewTodo onAddTodo={this.handleAddTodo}/>
                  <TodoList onDeleteTodo={this.handleDeleteTodo}
                            onCompleteTodo={this.handleCompleteTodo}
                            todoList={this.state.todoList}
                  />
                </div>
              </div>
            </div>

            <div className="col-sm-2 col-md-3"/>
          </div>
        </div>
    );
  }
}

export default TodoListApp;
