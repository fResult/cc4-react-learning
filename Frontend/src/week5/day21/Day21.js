import React, {Component} from 'react';
import TodoListApp from "./todo-app-recap-teacher-job/TodoListApp";

class Day21 extends Component {
  render() {
    return (
        <div>
          <h5>Day 21: {new Date('2019-11-19').toDateString()}</h5>
          <h6>Lab: Todo App Recap By Teacher Job</h6>
          <TodoListApp/>
        </div>
    );
  }
}

export default Day21;
