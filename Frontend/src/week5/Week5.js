import React from 'react';
import {Menu, Typography} from 'antd'
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  // useParams
} from "react-router-dom";

import Day20 from './day20/Day20';
import Day21 from './day21/Day21';
import Day23 from './day23/Day23';
import Day22 from "./day22/Day22";
import Day24 from "./day24/Day24";

const {Title} = Typography;

function Week5() {
  const match = useRouteMatch();

  return (
      <div>
        <div className="container-fluid">

          <Title level={3}>Week 5</Title>
          <Menu mode="vertical">
            <Menu.Item>
              <Link to={`${match.url}/day20`}>Day 20: React Basic Part 3: </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/day21`}>Day 21: Teacher Job teach TodoApp (Recap): </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/day22`}>Day 22: Redux Fundamental: </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/day23`}>Day 23: Redux with Counter app: </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/day24`}>Day 24: Recap Javascript Higher Order Function / Component: </Link>
            </Menu.Item>
          </Menu>

          <Switch>
            <Route path={`${match.path}/day20`}><Day20/></Route>
            <Route path={`${match.path}/day21`}><Day21/></Route>
            <Route path={`${match.path}/day22`}><Day22/></Route>
            <Route path={`${match.path}/day23`}><Day23/></Route>
            <Route path={`${match.path}/day24`}><Day24/></Route>
          </Switch>

          <hr/>
        </div>
      </div>
  );
}

export default Week5;
