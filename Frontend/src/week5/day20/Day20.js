import React from 'react';
import TodoList from "./todo-app-recap/TodoList";

export default class Day20 extends React.Component {
  render() {
    return (
        <div>
          <h5>Day 20: {new Date('2019-11-19').toDateString()}</h5>
          <h6>Lab & Homework: Todo App Recap: </h6>
          <TodoList/>
          <hr/>
        </div>
    )
  }
}
