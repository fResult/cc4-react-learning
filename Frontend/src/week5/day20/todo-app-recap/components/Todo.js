import React from 'react'

import styles from './Todo.module.css';

export default function Todo(props) {
  const {ticked = false, text = 'Pizza', id, onTick, onDelete} = props;
  return (
      <div className={styles.Root}>
        <span className={styles.Tick} style={ticked ? {background: '#A8D097'} : undefined} onClick={() => onTick(id)}/>
        <div className={styles.Text}
             style={{cursor: 'pointer', textDecorationLine: ticked ? 'line-through' : 'inherit'}}
             onClick={() => onTick(id)}
        >
          {text}
        </div>
        <span className={styles.Delete} onClick={() => onDelete(id)}>x</span>
      </div>
  )
}

// Todo.defaultProps = {
//   ticked: false,
//   name: 'pizza'
// };
