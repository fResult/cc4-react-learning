import React from 'react';

import styles from './NewTodo.module.css';

export default function NewTodo(props) {

  const {onValue, onAdd, value} = props;

  const keyUp = (e) => (e.key === 'Enter') ? onAdd() : null;

  return (
      <div className={styles.Root}>
        <input className={styles.Input} placeholder="Enter Todo" onKeyUp={keyUp} value={value} onChange={onValue}/>
        <button className={`btn ${styles.Button}`} onClick={onAdd}>Add</button>
      </div>
  )
}
