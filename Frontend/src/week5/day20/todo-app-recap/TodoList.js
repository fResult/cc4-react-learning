import React, {Component} from 'react';
// import PropTypes from 'prop-types';

import styles from './TodoList.module.css';

import NewTodo from './components/NewTodo';
import Todo from "./components/Todo";

class TodoList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      todoList: [
        {id: 1574087866275, ticked: true, text: 'Play football'},
        {id: 1574087873020, ticked: false, text: 'Say hi !'}
      ],
      textValue: ''
    }
  }

  handleValue = (e) => {
    console.log(Date.now())
    this.setState({textValue: e.target.value});
  };

  handleAddTodo = () => {
    if (!this.state.textValue) alert('Please Input Todo before submit');
    else {
      const {todoList} = this.state;
      this.setState({
        // todoList: todoList.concat({id: 3, text: this.state.textValue, ticked: false})
        textValue: '',
        todoList: [...todoList, {id: Date.now(), ticked: false, text: this.state.textValue}]
      });
    }
  };

  handleTickTodo = (id) => {
    this.setState({
      todoList: this.state.todoList.map(todo => {
        if (id === todo.id) return {id: todo.id, text: todo.text, ticked: !todo.ticked};
        else return todo;
      })
    })
  };

  handleDeleteTodo = (id) => {
    this.setState({
      todoList: this.state.todoList.filter(todo => todo.id !== id)
    });
  };

  render() {
    return (
        <div className={styles.Root}>
          <NewTodo value={this.state.textValue} onValue={this.handleValue} onAdd={this.handleAddTodo}/>
          {this.state.todoList.map((todo, idx) =>
              <Todo
                  key={idx}
                  id={todo.id}
                  ticked={todo.ticked}
                  text={todo.text}
                  onTick={this.handleTickTodo}
                  onDelete={this.handleDeleteTodo}
              />
          )}
        </div>
    );
  }
}

TodoList.propTypes = {};

export default TodoList;

