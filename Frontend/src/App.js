import './App.css';
import React from 'react';
import {Layout, Menu} from 'antd'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'

import Week4 from "./week4/Week4";
import Week5 from "./week5/Week5";
import Sandbox from "./Sandbox/Sandbox.js";
import Week6 from "./week6/Week6";
import Week7 from "./week7/Week7";


const {Header, Content, Footer} = Layout;

function App(props) {
  return (
      <Router>
        <Header style={{padding: 0, height: 'initial'}}>
          <Menu mode="horizontal" theme="dark">
            <Menu.Item key="sandbox">
              <Link to="/sandbox">Sandbox</Link>
            </Menu.Item>
            <Menu.Item key="week4">
              <Link to="/week4">Week4</Link>
            </Menu.Item>
            <Menu.Item key="week5">
              <Link to="/week5">Week5</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/week6">Week6</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/week7">Week7</Link>
            </Menu.Item>
          </Menu>
        </Header>
        <Content>
          <Switch>
            <Route path="/sandbox"><Sandbox/></Route>
            <Route path="/week4"><Week4/></Route>
            <Route path="/week5"><Week5/></Route>
            <Route path="/week6"><Week6/></Route>
            <Route path="/week7"><Week7/></Route>
          </Switch>
        </Content>
        <Footer>
          This is created by fResult. Educated in Code camp 4
        </Footer>
      </Router>
  );
}

export default App;
