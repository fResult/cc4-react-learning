import React from 'react';
import {
  Switch,
  Route,
  useRouteMatch,
  Link
} from 'react-router-dom'

const Day29 = (props) => {
  const match = useRouteMatch();

  return (
      <div>
        <h5>Day 29: {new Date('2019-11-29').toDateString()}</h5>
        <Link to={`${match.url}/lab`}>Lab: </Link>

        <Switch>
          <Route path={`${match.path}/lab`}>Lab</Route>
        </Switch>
      </div>
  );
};

export default Day29;
