import React, {Component} from 'react';
import Axios from 'axios';

class PostList extends Component {
  componentDidMount = async () => {
    const result = await Axios.get('http://localhost:3333/posts');
    this.setState({postList: result.data});
  };

  state = {
    postList: []
  };

  render() {
    return (
        <>
          {this.state.postList.map(post => <p>{post.content}</p>)}
        </>
    );
  }
}

export default PostList;
