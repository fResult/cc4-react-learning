import React from 'react';
import {Menu, Typography} from 'antd'
import {
  Switch,
  Route,
  useRouteMatch,
  Link
} from 'react-router-dom'
import Lab1AddAntComponents from "./lab/Lab1AddAntComponents";
import {WrappedLab2Login} from "./lab/Lab2Login";
import Lab3LoginPageWithGrid from "./lab/Lab3LoginPageWithGrid";
import Hw1SignUp from "./homework/Hw1SignUp";
import Hw2ChangePassword from "./homework/Hw2ChangePassword";
import Hw3ShoppingCart from "./homework/Hw3ShoppingCart";

const {Title} = Typography;
const Day26 = (props) => {
  const match = useRouteMatch();

  return (
      <div>
        <Title level={4}>Day 26: {new Date('2019-11-26').toDateString()}</Title>
        <Menu mode="horizontal">
          <Menu.Item>
            <Link to={`${match.url}/lab1`}>Lab 1: Add Antd Components</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`${match.url}/lab2`}>Lab 2: Login</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`${match.url}/lab3`}>Lab 3: Login with Grid</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`${match.url}/homework1`}>Homework 1: Sign Up Page</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`${match.url}/homework2`}>Homework 2: Change Password Page</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`${match.url}/homework3`}>Homework 3: Shopping Cart</Link>
          </Menu.Item>
        </Menu>

        <Switch>
          <Route path={`${match.path}/lab1`}><Lab1AddAntComponents/></Route>
          <Route path={`${match.path}/lab2`}><WrappedLab2Login form={{}}/></Route>
          <Route path={`${match.path}/lab3`}><Lab3LoginPageWithGrid/></Route>
          <Route path={`${match.path}/homework1`}><Hw1SignUp/></Route>
          <Route path={`${match.path}/homework2`}><Hw2ChangePassword/></Route>
          <Route path={`${match.path}/homework3`}><Hw3ShoppingCart/></Route>
        </Switch>
      </div>
  );
};

export default Day26;
