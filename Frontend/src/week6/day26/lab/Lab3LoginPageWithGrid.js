import React, {Component} from 'react';
import {Form, Icon, Button, Row, Col, Input} from "antd";

class Lab3LoginPageWithGrid extends Component {
  render() {
    return (
        <div>
          <div style={{display: 'flex', justifyContent: 'center'}}>
            <Row style={{height: '100%'}} type="flex" align="middle" justify="center">
              <Col span={12}>
                <div style={{maxWidth: '250px', marginRight: 20}}>
                  <img style={{width: '100%', height: '100%'}} alt=""
                       src="https://carlisletheacarlisletheatre.org/images/facebook-icons-clipart-transparency-png-transparent-background-7.png"
                  />
                </div>
              </Col>
              <Col span={12}>
                <Form onSubmit={this.handleSubmit} className="login-form">
                  <Form.Item>
                    <Input
                        prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        placeholder="Username"
                    />
                  </Form.Item>
                  <Form.Item>
                    <Input
                        prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        type="password"
                        placeholder="Password"
                    />
                  </Form.Item>
                  <Form.Item>
                    <Button style={{color: 'dodgerblue', border: 'none'}}>Sign in</Button>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                      Log in
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </div>
        </div>
    );
  }
}

export default Lab3LoginPageWithGrid;
