import React, {Component} from 'react';
import {Form, Icon, Input, Button} from 'antd';

class Lab2Login extends Component {


  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  }

  render() {
    return (
        <div>
          <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'center'}}>
            <div style={{width: 250, height: 250, marginRight: 20}}>
              <img style={{width: '100%', height: '100%'}} alt=""
                   src="https://carlisletheacarlisletheatre.org/images/facebook-icons-clipart-transparency-png-transparent-background-7.png"
              />
            </div>
            <div style={{height: 250, margin: 0, display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
              <Form onSubmit={this.handleSubmit} className="login-form">
                <Form.Item style={{marginBottom: '5px'}}>
                  Username:
                  <Input prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}/>
                </Form.Item>
                <Form.Item style={{marginBottom: '5px'}}>
                  Password
                  <Input
                      prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                  />
                </Form.Item>
                <Form.Item>
                  <Button style={{color: 'dodgerblue', border: 'none', marginRight: '15px'}}>Sign up</Button>
                  <Button type="primary" htmlType="submit" className="login-form-button">
                    Log in
                  </Button>
                </Form.Item>
              </Form>
            </div>
          </div>
        </div>
    );
  }
}

export const WrappedLab2Login = Form.create({name: 'normal_login'})(Lab2Login);
