import React, {Component} from 'react';
import {
  Form,
  Input,
  Row,
  Col,
  Button,
} from 'antd';

import styles from './Hw1SignUp.module.css';

class Hw1SignUp extends Component {
  state = {
  };

  render() {
    return (
        <div>
          <Row>
            <Col span={6}/>
            <Col span={12}>
              <Row style={{flexWrap: 'no-wrap', display: 'flex', flexDirection: 'column', alignItems: 'center'}}
                   type="flex" justify="center"
              >
                <div style={{maxWidth: 250}}>
                  <img style={{width: '100%'}}  alt=""
                       src="https://carlisletheacarlisletheatre.org/images/facebook-icons-clipart-transparency-png-transparent-background-7.png"
                  />
                </div>
                <Form onSubmit={this.handleSubmit} className="login-form">
                  <Form.Item>
                    <div>Email:</div>
                    <Input className={styles.Input}/>
                  </Form.Item>
                  <Form.Item>
                    Password:
                    <Input className={styles.Input}/>
                  </Form.Item>
                  <Form.Item>
                    Confirm Password:
                    <Input className={styles.Input}/>
                  </Form.Item>
                  <Form.Item>
                    Name:
                    <Input className={styles.Input}/>
                  </Form.Item>
                  <Form.Item style={{textAlign: 'center'}}>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                      Sign Up
                    </Button>
                  </Form.Item>
                </Form>
              </Row>
            </Col>
            <Col span={6}/>
          </Row>
        </div>
    )
  }
}

export default Hw1SignUp;
