import React, {Component} from 'react';
import {Typography} from 'antd/lib/index'

import styles from './Profile.module.css'

const {Title} = Typography;

class Profile extends Component {
  render() {
    return (
        <div className={styles.Root}>
          <div>
            <img className={styles.ProfileIcon} alt=""
                 src="https://scontent.fbkk10-1.fna.fbcdn.net/v/t1.0-9/21430334_259349151252216_9028693015125120582_n.jpg?_nc_cat=102&_nc_eui2=AeEVgs3uuj4vKOX68YqiOrOvYbJHU7e9h1BPsSPrs0kjZEuMu6hq8sxfUz_8Zy47w8anAPDOoj-LIxhXN62-Dh9hLd1CsWdvXBi8j5PqpwvnrQ&_nc_ohc=TKGLZqXVkC8AQk2Yqaewmw3eiujDxeD4tnR-fV5jWOLaMRL6ZrkK8NRMA&_nc_ht=scontent.fbkk10-1.fna&oh=94e6bf7ef6512c1ba55f25a30e09eb5e&oe=5E48E302"
            />
          </div>
          <div style={{marginLeft: 77}}>
            <Title level={1} className={styles.NameProfile}>Sila</Title>
            <Title level={1} className={styles.NameProfile}>Setthakan-anan</Title>
          </div>
        </div>
    );
  }
}

export default Profile;
