import React, {Component} from 'react';
import {Row, Col, Button, Divider, Input, Form, Typography} from 'antd/lib/index'

import styles from './ChangePassword.module.css';
import Profile from './Profile';

const {Text} = Typography;

class ChangePassword extends Component {
  render() {
    return (
        <div className={styles.Root}>
          <div>
            <Row style={{margin: '0 0 0 80px', display: 'flex'}}>
              <Profile/>
            </Row>
            <Divider/>
            <Row>
              <Col span={6}/>
              <Col span={12}>
                <Form layout="inline" onSubmit={this.handleSubmit}>
                  <Form.Item>
                    <Text strong style={{fontSize: 36}}>รหัสผ่านเดิม: </Text>
                    <Input className={styles.ChangePasswordInput}/>
                  </Form.Item>
                  <Form.Item>
                    <Text strong style={{fontSize: 36}}>รหัสผ่านใหม่: </Text>
                    <Input className={styles.ChangePasswordInput}/>
                  </Form.Item>
                  <Form.Item>
                    <Text strong style={{fontSize: 36}}>รหัสผ่านใหม่อีกครั้ง: </Text>
                    <Input className={styles.ChangePasswordInput}/>
                  </Form.Item>
                  <Form.Item>
                    <Button type="primary">
                      เปลี่ยนรหัส
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
              <Col span={6}/>
            </Row>
          </div>
        </div>
    );
  }
}

export default ChangePassword;
