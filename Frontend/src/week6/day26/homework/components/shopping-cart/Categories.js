import React, {Component} from 'react';
import {Menu} from 'antd';

class Categories extends Component {
  constructor(props) {
    super(props);
    this.renderCategories = this.renderCategories.bind(this);
  }

  renderCategories() {
    const {categories, onCategoriesId} = this.props;
    return categories.map(category => (
        <Menu.Item key={category.id} onClick={(e) => onCategoriesId(parseInt(e.key))}>
          {category.name}
        </Menu.Item>
    ))
  }

  render() {
    const cateId = this.props.selectedId;
    console.log(cateId, 'cateId');
    return (
        <div>
          <Menu
              selectedKeys={[(cateId === null) ? null : cateId.toString()]}
              defaultSelectedKeys={['1']}
              mode="inline"
          >
            {this.renderCategories()}
          </Menu>
        </div>
    );
  }
}

export default Categories;
