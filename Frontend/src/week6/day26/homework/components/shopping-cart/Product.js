import React, {Component} from 'react';

import {Row, Card, Typography, Button} from 'antd';

const {Text, Title} = Typography;

class Product extends Component {
  render() {
    const {product, onClickAddToCart} = this.props;
    return (
        <Card
            hoverable
            cover={<img alt="example" src={product.image}/>}
            style={{width: 400, height: 700, margin: '15px 1px'}}
        >
          <Title level={4}>{product.name}</Title>
          <div style={{maxHeight: 200, overflowY: 'scroll'}}>
            <Text>{product.description}</Text>
          </div>
          <Row>
            <Text code> {product.price}</Text>
            <Button onClick={() => onClickAddToCart(product)}>Add to cart</Button>
          </Row>
        </Card>
    );
  }
}

export default Product;
