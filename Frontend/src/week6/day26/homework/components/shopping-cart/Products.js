import React, {Component} from 'react';
import Product from "./Product";

export default class Products extends Component {
  render() {
    return (
        <div style={{display: 'flex', flexWrap: 'wrap'}}>{
          this.props.products.map(product => (
              <Product key={product.id} product={product} onClickAddToCart={this.props.onClickAddToCart}/>
          ))
        }</div>
    );
  }
}

