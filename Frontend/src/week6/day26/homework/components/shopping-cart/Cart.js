import React, {Component} from 'react';

import {Table, Row, Col, Button, Statistic} from 'antd';

import Axios from 'axios';

class Cart extends Component {
  handleCheckoutCart = async (totalPrice) => {
    try {
      await Axios.post('http://localhost:3030/add-order', {
        totalPrice: totalPrice,
        orderList: this.props.cart
      })
      this.props.onDeleteProductFromCart();
    } catch (ex) {
      console.error(ex.message)
    }
  }

  render() {
    const columns = [
      {title: 'Name', dataIndex: 'product.name'},
      {title: 'Price', dataIndex: 'product.price'},
      {title: 'Amount', dataIndex: 'amount'},
      {
        title: 'Action',
        dataIndex: '',
        render: (text, cartItem) => (
            <Button type="danger" onClick={() => this.props.onDeleteProductFromCart(cartItem.product.id)}>
              Delete
            </Button>
        )
      },
    ];

    let total = 0;
    const cartItems = this.props.cart;
    cartItems.forEach(cartItem => {
      total += cartItem.amount * cartItem.product.price
    });

    return (
        <Row>
          <Col>
            <Row>
              <Table rowKey={() => this.props.cart.map(x => x)}
                     columns={columns} dataSource={this.props.cart} bordered
              />
            </Row>
            <Row type="flex" justify="end">
              <Statistic title="Total price" value={total} precision={2}/>
            </Row>
            <Row type="flex" justify="end">
              <Button
                  onClick={() => this.handleCheckoutCart(total)}
                  style={{marginTop: 16}} type="primary"
              >
                Check out
              </Button>
            </Row>
          </Col>
        </Row>
    );
  }
}

export default Cart;
