import React, {Component} from 'react';
import {Row, Col} from "antd";
import {uniqueId} from 'lodash';
import Axios from 'axios';

import Categories from "./components/shopping-cart/Categories";
import Products from "./components/shopping-cart/Products";
import Cart from "./components/shopping-cart/Cart";

class Hw3ShoppingCart extends Component {

  componentDidMount = async () => {
    const categories = await Axios.get('http://localhost:3030/product-category');
    const products = await Axios.get('http://localhost:3030/product');
    this.setState({
      products: products.data,
      categories: categories.data,
      selectedCategoriesId: categories.data[0].id
    })
  };

  constructor(props) {
    super(props);
    this.state = {
      cart: [],
      selectedCategoriesId: null,
      categories: [],
      products: []
    };
    this.handleCategoriesId = this.handleCategoriesId.bind(this);
    this.handleClickAddToCart = this.handleClickAddToCart.bind(this);
    this.handleDeleteProductFromCart = this.handleDeleteProductFromCart.bind(this);
    this.handleDeleteAllProductsFromCart = this.handleDeleteAllProductsFromCart.bind(this)
  }

  handleCategoriesId = (id) => {
    this.setState({selectedCategoriesId: id});
  };

  filterProducts() {
    const id = this.state.selectedCategoriesId;

    if (id === null) {
      return [];
    } else {
      return this.state.products.filter(product => product.ProductCategoryId === id)
    }
  }

  handleClickAddToCart(product) {
    if (this.state.cart.find(cartItem => cartItem.product.id === product.id)) {
      this.setState({
        cart: this.state.cart.map(cartItem =>
            cartItem.product.id === product.id ? {...cartItem, amount: ++cartItem.amount} : cartItem
        )
      })
    } else {
      this.setState({cart: [...this.state.cart, {uid: uniqueId(), product, amount: 1}]})
    }
  }

  handleDeleteProductFromCart(productId) {
    const {cart} = this.state;
    this.setState({
      cart: cart.filter(cartItem => cartItem.product.id !== productId)
    });
  }

  handleDeleteAllProductsFromCart() {
    this.setState({cart: []})
  }

  render() {
    return (
        <>
          <Row type="flex">
            <Col span={3}>
              <Categories
                  selectedId={this.state.selectedCategoriesId}
                  onCategoriesId={this.handleCategoriesId}
                  categories={this.state.categories}/>
            </Col>
            <Col span={13}>
              <Products products={this.filterProducts()} onClickAddToCart={this.handleClickAddToCart}/>
            </Col>
            <Col span={6}>
              <Cart cart={this.state.cart} onDeleteProductFromCart={this.handleDeleteProductFromCart}
                    onDeleteAllProductsFromCart={this.handleDeleteAllProductsFromCart}/>
            </Col>
          </Row>
        </>
    );
  }
}

export default Hw3ShoppingCart;
