import React, {Component} from 'react';
import {Layout} from 'antd';

// import styles from './Hw2ChangePassword.module.css';
import ChangePassword from "./components/facebook/ChangePassword";
import NavBar from "./components/facebook/NavBar";

const {Header, Content} = Layout;

class Hw2ChangePassword extends Component {
  render() {
    return (
        <div>
          <Layout style={{margin: '0 -15px'}}>
            <Header style={{padding: 0}}>
              <NavBar/>
            </Header>
            <Content>
              <ChangePassword/>
            </Content>
          </Layout>
        </div>
    );
  }
}

export default Hw2ChangePassword;
