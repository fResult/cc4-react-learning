import React from 'react';
import {Menu, Typography} from 'antd'
import {
  Switch,
  Route,
  useRouteMatch,
  Link
} from 'react-router-dom'

import Parent from "./lab/tab-component/Parent";
import SlideShowParent from "./lab/slide-show/SlideShowParent";
import SearchList from "./homework/SearchList";

const {Title} = Typography;

const Day25 = () => {
  const match = useRouteMatch();
  return (
      <div>
        <Title level={4}>Day 25: {new Date('2019-11-25').toDateString()}</Title>
        <Menu mode="horizontal">
          <Menu.Item>
            <Link to={`${match.url}/lab-tab-component`}>Lab: Tab Component</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`${match.url}/lab-slide-show`}>Lab: Slide Show</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to={`${match.url}/search-list`}>Homework: Search List</Link>
          </Menu.Item>
        </Menu>

        <Switch>
          <Route path={`${match.path}/lab-tab-component`}><Parent/></Route>
          <Route path={`${match.path}/lab-slide-show`}><SlideShowParent/></Route>
          <Route path={`${match.path}/search-list`}><SearchList/></Route>
        </Switch>
      </div>
  );
};

export default Day25;
