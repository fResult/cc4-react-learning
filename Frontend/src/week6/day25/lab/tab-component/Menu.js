import React from 'react';

import styles from './Menu.module.css';

const Menu = (props) => {
  const {onChangeTab} = props;

  return (
      <div>
        <nav>
          <ul className={styles.Menu}>{
            props.menu.map((item, idx) => (
                <button key={idx} className={styles.MenuItem}
                        style={item.id !== idx+1 ? {borderStyle: 'inset'} : {borderStyle: 'outset'}}
                        onClick={() => onChangeTab(idx)}>
                  <li>
                    {item.name}
                  </li>
                </button>
            ))
          }</ul>
        </nav>
      </div>
  );
};

export default Menu;
