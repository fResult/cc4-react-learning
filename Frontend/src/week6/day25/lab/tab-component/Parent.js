import React, {Component} from 'react';

import Menu from './Menu';
import Body from "./Body";

class Parent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      menu: [{id: 1, name: 'Home'}, {id: 2, name: 'Menu 1'}, {id: 3, name: 'Menu 2'}, {id: 4, name: 'Menu 3'}],
      bodies: [
        {title: 'Home', desc: 'Home Home Home Home Home Home Home Home Home Home Home Home Home Home Home Home'},
        {title: 'Menu 1', desc: 'Menu1 Menu1 Menu1 Menu1 Menu1 Menu1 Menu1 Menu1 Menu1 Menu1 Menu1 Menu1 Menu1'},
        {title: 'Menu 2', desc: 'Menu2 Menu2 Menu2 Menu2 Menu2 Menu2 Menu2 Menu2 Menu2 Menu2 Menu2 Menu2 Menu2'},
        {title: 'Menu 3', desc: 'Menu3 Menu3 Menu3 Menu3 Menu3 Menu3 Menu3 Menu3 Menu3 Menu3 Menu3 Menu3 Menu3'}
      ]
    }
  }

  body = {};

  handleChangeTab = (idx) => {
    const {bodies} = this.state;
    this.body = bodies[idx];
    this.setState({
      bodies: [
        ...bodies, {...bodies[idx]}
      ]
    });
  };

  render() {
    return (
        <div>
          <h6>Tab Component Lab</h6>
          <Menu menu={this.state.menu} onChangeTab={this.handleChangeTab}/>
          <Body body={this.body}/>
        </div>
    );
  }
}

export default Parent;
