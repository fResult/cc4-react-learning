import React, {Component} from 'react';

class Body extends Component {
  componentDidMount() {
    this.setState({body: this.props.body})
  }

  constructor(props) {
    super(props);
    this.state = {
      body: {title: 'Home', desc: 'Home Home Home Home Home Home Home Home Home Home Home Home Home Home Home Home'}
    }
  }

  render() {
    return (
        <div style={{border: '2px dodgerblue solid', padding: 15, height: 98}}>
          <header>{this.props.body.title}</header>
          <p>{this.props.body.desc}</p>
        </div>
    );
  }
}

export default Body;
