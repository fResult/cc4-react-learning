import React, {Component} from 'react';

import styles from './SlideShowParent.module.css';

class SlideShowParent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
      images: [
        {name: 'Sunset', src: 'https://cdn.pixabay.com/photo/2015/06/19/20/13/sunset-815270_1280.jpg'},
        {name: 'Rainy', src: 'https://cdn.pixabay.com/photo/2016/06/25/17/36/rain-1479303_1280.jpg'},
        {name: 'Wave', src: 'https://cdn.pixabay.com/photo/2015/09/02/13/19/ocean-918999_1280.jpg'},
        {name: 'City', src: 'https://cdn.pixabay.com/photo/2017/03/29/15/18/tianjin-2185510_1280.jpg'}
      ]
    }
  }

  handlePreviousPic = () => {
    let {currentIndex} = this.state;
    if (currentIndex <= 0) return;
    this.setState({currentIndex: --currentIndex})
  };

  handleNextPic = () => {
    let {currentIndex, images} = this.state;
    if (currentIndex >= images.length - 1) return;
    this.setState({currentIndex: ++currentIndex});
  };

  render() {
    const {currentIndex, images} = this.state;
    return (
        <div className={styles.Parent}>
          <div>
            <div style={{width: 400, height: 264.06}}>
              <img style={{width: 400}} src={this.state.images[this.state.currentIndex].src} alt={this.state.name}/>
            </div>
            <div className={styles.Buttons}>
              <button onClick={this.handlePreviousPic}
                      disabled={currentIndex <= 0}>Previous
              </button>
              <button onClick={this.handleNextPic}
                      disabled={currentIndex >= images.length - 1}>Next
              </button>
            </div>
          </div>
        </div>
    );
  }
}

export default SlideShowParent;
