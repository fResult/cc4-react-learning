import React, {Component} from 'react';

class SearchList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      movies: [
        {name: 'Spider man'},
        {name: 'Iron man'},
        {name: 'Thor'},
        {name: 'Hulk'},
        {name: 'Ant man'},
        {name: 'Scarlet'},
        {name: 'Pac man'},
      ]
    }
  }

  handleSearch = (e) => {
    this.setState({search: e.target.value})
  };

  render() {
    const {movies, search} = this.state;
    return (
        <div style={{
          height: '100vh',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          <input onChange={this.handleSearch}/>
          <span style={{textAlign: 'right'}}>
            &nbsp;{`${search.length} character${(search.length > 1) ? 's' : ''}`}
          </span>
          <ul>
            {movies.map((movie, idx) => (
                (search === '' || search === null) ? <li key={idx}>{movie.name}</li> :
                    movie.name.toLowerCase().replace(' ', '').includes(search.trim()) ?
                        <li key={idx}>{movie.name}</li> :
                        ''
            ))}
          </ul>
        </div>
    );
  }
}

export default SearchList;
