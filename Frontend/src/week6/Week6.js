import React from 'react';
import {Menu, Typography} from 'antd'
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
} from "react-router-dom";
import Day25 from "./day25/Day25";
import Day27 from "./day27/Day27";
import Day28 from "./day28/Day28";
import Day29 from "./day29/Day29";
import Day26 from "./day26/Day26";
import RecapSunday from "./recap-sunday/RecapSunday";

const {Title} = Typography;

const Week6 = (props) => {
  const match = useRouteMatch();

  return (
      <div>
        <div className="container-fluid">
          <Title level={3}>Week 6</Title>
          <Menu mode="vertical">
            <Menu.Item>
              <Link to={`${match.url}/day25`}>Day 25: Recap React by Teacher Oak.</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/day26`}>Day 26: Ant Design</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/day27`}>Day 27: Continue of day 26</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/day28`}>Day 28: </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/day29`}>Day 29: </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/recap-sunday`}>Recap Sunday: Post-Comment Application</Link>
            </Menu.Item>
          </Menu>

          <Switch>
            <Route path={`${match.path}/day25`}><Day25/></Route>
            <Route path={`${match.path}/day26`}><Day26/></Route>
            <Route path={`${match.path}/day27`}><Day27/></Route>
            <Route path={`${match.path}/day28`}><Day28/></Route>
            <Route path={`${match.path}/day29`}><Day29/></Route>
            <Route path={`${match.path}/recap-sunday`}><RecapSunday/></Route>
          </Switch>
        </div>
      </div>
  );
};

export default Week6;
