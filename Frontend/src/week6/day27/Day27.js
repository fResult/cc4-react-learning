import React from 'react';

const Day27 = (props) => {
  return (
      <div>
        <h5>Day 27: {new Date('2019-11-27').toDateString()}</h5>
        <h6>Study to Create Shopping Cart (Homework Day 26)</h6>
      </div>
  );
};

export default Day27;
