import {createStore} from 'redux'
import {rootReducer} from '../reducers/reducers';

let initialState = {
  notes: []
};

const notes = JSON.parse(localStorage.getItem('notes'));
if (notes) {
  initialState = notes;
}

export default createStore(rootReducer, initialState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
