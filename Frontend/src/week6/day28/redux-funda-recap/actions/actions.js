import {uniqueId} from 'lodash'

export const ADD_NOTE = 'ADD_NOTE';
export const DELETE_NOTE = 'DELETE_NOTE';

// Action Creator
export function addNote(title, content) {
  return {type: ADD_NOTE, title: title, content: content, id: uniqueId('note-')}
}

export function delNote(id) {
  return {type: DELETE_NOTE, id: id}
}
