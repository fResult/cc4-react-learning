import {addNote, delNote} from './actions/actions';
import store from './store/store';

// ------ HTML references ------
let notesUList = document.getElementById('notes');
let addNoteForm = document.getElementById('add-note');
let addNoteTitle = addNoteForm['title'];
let addNoteContent = addNoteForm['content'];


function setDeleteNoteButtonsEventListeners() {
  let buttons = document.querySelectorAll('ul#notes li button');

  for (let button of buttons) {
    button.addEventListener('click', () => {
      deleteNote(button.dataset.id);
    });
  }
}


// ------ Redux ------

store.subscribe(() => {
  localStorage.setItem('notes', JSON.stringify(store.getState()));
  renderNotes()
});

function deleteNote(id) {
  let action = delNote(id)
  store.subscribe(() => renderNotes());
  store.dispatch(action);
}

// function searchNotes() {
  const input = document.querySelector('#search');
  input.addEventListener('keyup', () => {
    console.log(input.value)
    let {notes} = store.getState();
    return  notes.filter(note => note.content.includes(input.value) || note.title.includes(input.value));
  });
// }

function renderNotes() {
  notesUList.innerHTML = '';

  let notes = store.getState().notes;
  notes.forEach(note => {
    notesUList.innerHTML += `
       <li>
          <b>${note.title}</b>
          <button data-id="${note.id}">X</button>
          <br/>
          ${note.content}
       </li>
    `;
  });
  setDeleteNoteButtonsEventListeners()
}


// ------ Event Listeners ------
addNoteForm.addEventListener('submit', (e) => {
  e.preventDefault();
  console.log('hello')

  let action = addNote(addNoteTitle.value, addNoteContent.value);
  store.dispatch(action);
});


// ------ Render the initial Notes ------
renderNotes();
