import {ADD_NOTE, DELETE_NOTE} from "../actions/actions";

const initialState = {
  notes: []
};

export function rootReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_NOTE:
      return {
        notes: [...state.notes, {id: action.id, title: action.title, content: action.content}]
      };
    case DELETE_NOTE:
      return {
        notes: state.notes.filter(note => note.id !== action.id)
      };
    default:
      return state
  }
}
