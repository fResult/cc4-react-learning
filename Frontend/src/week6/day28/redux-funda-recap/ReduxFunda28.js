import React from 'react';

const ReduxFunda28 = () => {
  return (
      <div>
        <script src="./main.js" type="text/javascript"/>
        <h1>Redux Notes App</h1>

        <h3>Add a Note</h3>
        <form id="add-note">
          Title: <br/>
          <input type="text" name="title"/>
          <br/>
          Content: <br/>
          <textarea name="content" cols="30" rows="5"/>
          <br/>
          <button type="button" onSubmit={() => false}>Add Note</button>
        </form>

        <hr/>

        <h3>All Notes</h3> <input type="text" id="search" data-id=""/>
        <ul id="notes">
          <li>
            <b>Title</b>
            <button data-id="5">x</button>
            <br/>
            <span>Note Content</span>
          </li>
        </ul>
      </div>
  )
};

export default ReduxFunda28;
