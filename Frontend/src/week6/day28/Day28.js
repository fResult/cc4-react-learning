import React from 'react';
import {Menu, Typography} from 'antd'
import {
  Switch,
  Route,
  useRouteMatch,
  Link
} from 'react-router-dom'
import ReduxFunda28 from "./redux-funda-recap/ReduxFunda28";


const {Title} = Typography;
const Day28 = (props) => {
  const match = useRouteMatch();

  return (
      <div>
        <Title level={4}>Day 28: {new Date('2019-11-28').toDateString()}</Title>
        <Menu mode="horizontal">
          <Menu.Item>
            <Link to={`${match.url}/recap-redux-funda-28`}>Recap: Redux Fundamental by Job Teacher.</Link>
          </Menu.Item>

          <Switch>
            <Route path={`${match.path}/recap-redux-funda-28`}><ReduxFunda28/></Route>
          </Switch>
        </Menu>


      </div>
  );
};

export default Day28;
