import React from 'react';
import {Menu, Typography} from 'antd'
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  // useParams
} from "react-router-dom";

import Day19 from "./day19/Day19.js";
import Day18 from "./day18/Day18";

const {Title} = Typography;

export default function Week4() {
  const match = (useRouteMatch('/week4'));

  return (
      <div>
        <div className="container-fluid">
          <Title level={3}>Week 4</Title>
          <Menu mode="horizontal">
            <Menu.Item>
              <Link to={`${match.url}/day18`}>Day 18: React Basic Part 1: </Link>
            </Menu.Item>
            <Menu.Item>
              <Link to={`${match.url}/day19`}>Day 19: React Basic Part 2: </Link>
            </Menu.Item>
          </Menu>
        </div>
        <Switch>
          <Route path={`${match.path}/day18`}><Day18/></Route>
          <Route path={`${match.path}/day19`}><Day19/></Route>
        </Switch>
      </div>
  )
}

