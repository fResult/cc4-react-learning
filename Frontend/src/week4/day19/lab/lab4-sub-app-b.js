import React from 'react';

export default class AppB extends React.Component {
  state = {
    hasBracket: true,
    skills: [
      {id: 3, name: 'Java'},
      {id: 4, name: 'C++'},
      {id: 5, name: 'Swift'}
    ]
  };

  handleChangeSkill = () => {
    this.setState({
      hasBracket: !this.state.hasBracket,
    })
  };

  render() {
    return (
        <button onClick={this.handleChangeSkill}>
          change
          skills: {this.state.hasBracket === true ? this.state.skills.map(s => s.name) :
            `(${this.state.skills.map(s => s.name)})`}
        </button>
    )
  }
}
