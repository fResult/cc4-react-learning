import React from 'react';

class Lab2 extends React.Component {
  listItem = ['Subwoofer', 'Non-prous, washable', 'Wings', 'Beveled Bezel', 'Bezeled Bevel', 'Seedless'];

  render() {
    return (
        <ul>
          {this.listItem.map((item, idx) => <li key={idx}>{item}</li>)}
        </ul>
    )
  }
}

export default Lab2;
