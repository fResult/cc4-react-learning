import React from 'react';

export default class AppA extends React.Component {
  state = {name: 'John'};

  handleChangeName = () => {
    let name = '';
    if (this.state.name === 'John') name = 'Smith';
    else if (this.state.name === 'Smith') name = 'John';

    this.setState({name: name});
  };

  render() {
    return (
        <button onClick={this.handleChangeName}>
          change name: {this.state.name}
        </button>
    )
  }

}
