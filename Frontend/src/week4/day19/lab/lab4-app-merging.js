import React from 'react';

import AppA from './lab4-sub-app-a'
import AppB from './lab4-sub-app-b'

export default class Lab4 extends React.Component {
  state = {
    name: 'john',
    skills : [
        {id: 3, name: 'Java'},
        {id: 4, name: 'C++'},
        {id: 5, name: 'Swift'}
      ]
  };

  render() {
    return (
        <div>
          <AppA name={this.state.name}/>
          <AppB skills={this.state.skills}/>
        </div>
    )
  }
}
