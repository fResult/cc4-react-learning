import React from 'react';

export default function Lab3(props) {
  return (
      <div>
        <button onClick={click}>On Click</button>
        <br/>
        <input name="input-name" onKeyUp={keyUp} onChange={change}/>
        <br/>

      </div>
  )
}

function click() {
  alert('onClick is working!')
}

function change({target: {name, value}}) {
  console.log(`value: ${value} | name: ${name}`)
}

function keyUp(e) {
  e.key !== 'Alt' ?  console.log(`key is : ${e.key}`) : alert(`Don't press ALT button.`);
}
