import React from 'react';
import Lab2 from './lab/lab2-map-to-jsx.js';
import Lab3 from './lab/lab3-event-experiment';
import Lab4 from './lab/lab4-app-merging'
import TodoApp from "./homework-todo-app/TodoApp";

export default class Day19 extends React.Component {
  render() {
    return (
        <div>
          <h5>Day 19: {new Date('2019-11-15').toDateString()}</h5>
          <h6>Lab 2: Map array to JSX</h6>
          <Lab2/>
          <hr/>

          <h6>Lab 3: Event Experiment</h6>
          <Lab3/>
          <hr/>

          <h6>Lab 4: App Merging</h6>
          <Lab4/>
          <hr/>

          <h6>Homework: Todo App</h6>
          <TodoApp/>
          <hr/>
        </div>
    )
  }
}
