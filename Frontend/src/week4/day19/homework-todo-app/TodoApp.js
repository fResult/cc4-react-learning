import React from 'react';

import Add from './Add';
import List from './List';
import './TodoApp.css';

export default class TodoApp extends React.Component {
  state = {
    todoList: [
      {id: 1573880956433, text: 'Play football', completed: false},
      {id: 1573881020015, text: 'Exercise', completed: false},
      {id: 1576920190219, text: 'Do yoga', completed: false},
      {id: 1719492019306, text: 'Swimming', completed: false}
    ],
  };

  handleAddTodo = (todo) => {
    this.setState(state => state.todoList.push(todo));
  };

  handleUpdateTodo = (todo) => {
    console.log("handleUpdateTodo is working!");
  };

  handleDeleteTodo = (todoId) => {
    this.state.todoList.forEach((todo, idx) => {
      if (todoId === todo.id) this.setState(this.state.todoList.splice(idx, 1))
    })
  };

  handleCompleteTodo = (todoId) => {
    this.state.todoList.forEach((todo, idx) => {
        if (todoId === todo.id) {
          todo.completed = !todo.completed;
          this.setState(() => {
            this.state.todoList.splice(idx, 1, todo)
          });
        }
    })
  };

  render() {
    return (
        <div className="todo-app">
          <div className="row">
            <div className="col-md-3">
            </div>
            <div className="col-md-6">
              <div className="container todo-app--container">
                <div className="card todo-app--card">
                  <div className="card-body todo-app--card-body">
                    <Add addTodo={this.handleAddTodo}/>
                    <List todoList={this.state.todoList}
                          changeTodo={this.handleUpdateTodo}
                          deleteTodo={this.handleDeleteTodo}
                          completeTodo={this.handleCompleteTodo}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-3">
            </div>
          </div>
        </div>
    )
  }
}
