import React from 'react';

import './List.css';

export default class List extends React.Component {
  state = {
    isCompleted: false,
  };

  onDeleteTodo(id) {
    this.props.deleteTodo(id);
  };

  onUpdateTodo() {
    alert('Update is not do yet!')
  };

  onComplete(e, todoId) {
    this.setState({isCompleted: !this.state.isCompleted});
    this.props.completeTodo(todoId);
  };

  render() {
    return (
        <div className="todo-app_list">
          <ul className="list-group todo-app_list--list-group">{
            this.props.todoList.map((todo) => (
                <li className="list-group-item list-group-item-action todo-app_list--list-item"
                    key={todo.id} /*onDoubleClick={}*/>
                  <div className="row">
                    <div className="col-auto todo-app_list--tick-item">
                      <i className={`far fa-circle`} onClick={(e) => this.onComplete(e, todo.id)}
                         style={todo.completed === true ? {
                           width: 16,
                           height: 16,
                           background: 'green',
                           borderRadius: '50%'
                         } : null}
                      />
                    </div>
                    <div className="col todo-app_list--text-item">
                      <span style={todo.completed === true ? {textDecorationLine: 'line-through'} : null}>
                        {todo.text}
                      </span>
                    </div>
                    <div className="col-auto">
                      <button className="btn" onClick={() => this.onUpdateTodo(todo.id)}>
                        <i className="fas fa-edit"/>
                      </button>
                      <button className="btn" onClick={() => this.onDeleteTodo(todo.id)}>
                        <i className="fas fa-trash-alt"/>
                      </button>
                    </div>
                  </div>
                </li>
            ))
          }</ul>
        </div>
    )
  }
}
