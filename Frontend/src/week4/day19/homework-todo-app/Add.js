import React from 'react';

import './Add.css'
export default class Add extends React.Component {
  state = {
    todoList: []
  };

  onAddTodo = (e) => {
    let input = document.querySelector(e.target.getAttribute('name'));
    // let input = document.querySelector('.todo-app_add--input');
    if (input.value === null || input.value === '') {
      alert('Please input Todo before add !');
    } else {
      this.props.addTodo({id: Date.now(), text: input.value, completed: false});
      input.value = '';
    }
  };

  render() {
    return (
        <div className="todo-app_add">
          <div className="input-group">
            <div className="container-fluid">
              <div className="row">
                <div className="col-auto todo-app_add--left">
                  <input className="todo-app_add--input form-control" placeholder="Enter Todo"/>
                </div>
                <div className="col-auto todo-app_add--right">
                  <div className="input-group-append">
                    <button name=".todo-app_add--input" className="btn todo-app_add--btn-add" onClick={this.onAddTodo}>
                      Add
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    )
  }
}
