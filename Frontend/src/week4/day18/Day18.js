import React from 'react';

import Postcard from "./lab/Postcard";
import Popup from "./homework/popup/Popup";
import Trillo from "./homework/trillo/Trillo";

export default class Day18 extends React.Component {
  render() {
    return (
        <div>
          <h5>Day 18: {new Date('2019-11-14').toDateString()}</h5>

          <h6>Lab: Hello</h6>
          <div style={{textAlign: 'center'}}>{'Hello World'}</div>
          <hr/>

          <h6>Lab: Postcard</h6>
          <Postcard borderColor="black" size="50px" dotColor="dodgerblue"/>
          <hr/>

          <h6>Homework 1: Popup</h6>
          <Popup heading="This Is Important">
            Here is some important...
          </Popup>
          <hr/>

          <h6>Homework 2: Trillo</h6>
          <Trillo/>

          <hr/>
        </div>
    )
  }
}
