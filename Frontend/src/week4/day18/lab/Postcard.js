import React from 'react';

export default class Postcard extends React.Component {
  red50 = {color: 'red', fontSize: this.props.size}
  green50 = {color: 'green', fontSize: this.props.size}
  dot50 = {color: this.props.dotColor, fontSize: this.props.size}

  render() {
    return (
        <div style={{
          display: "inline-block",
          border: `${this.props.borderColor} solid 1px`
        }}>
          <span style={this.red50}>L</span>
          <span style={this.dot50}>.</span>
          <span style={this.green50}>O</span>
          <span style={this.dot50}>.</span>
          <span style={this.red50}>V</span>
          <span style={this.dot50}>.</span>
          <span style={this.green50}>E</span>
        </div>
    )
  }
}

