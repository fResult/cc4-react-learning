import React from 'react';
import './Popup.css';

class Popup extends React.Component {
  render() {
    return (
        <div className="container">
          <div className="card">
            <div className="card-header">
              <h5 style={{margin: 0}}>{this.props.heading}</h5>
            </div>
            <div className="card-body">
              {this.props.children}
            </div>
            <div className="card-footer">
              <div className="container-fluid">
                <button className="btn btn-outline-dark">Close</button>
              </div>
            </div>
          </div>
        </div>
    )
  }
}

export default Popup;
