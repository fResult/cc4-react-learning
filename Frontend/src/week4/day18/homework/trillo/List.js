import React from 'react';
import './List.css';

class List extends React.Component {
  render() {
    return (
        <ul id="phone-features" className="list-group" style={{listStyle: 'none'}}>{
          this.props.items.map(item => (
              <li key={item.id} className="list_feature">
                <input type="text" disabled value={item.text} className="list_feature list-group-item-action"/>
              </li>
          ))
        }</ul>
    );
  }
}

export default List;
