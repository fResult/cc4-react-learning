import React from 'react';
import './Trillo.css';
import List from './List.js';

class Trillo extends React.Component {
  phoneFeatures = [
    {id: 1, text: 'Subwoofer'},
    {id: 2, text: 'Non-porous, washable'},
    {id: 3, text: 'Wings'},
    {id: 4, text: 'Beveled Bezel'},
    {id: 5, text: 'Bezeled Bevel'},
    {id: 6, text: 'Seedless'},
  ];

  render() {
    return (
        <div className="container trillo">
          <div className="row">
            <div className="col-md-3"/>
            <div className="col-md-6">
              <div className="card">
                <div className="card-header">
                  <h5>Phone Features</h5>
                </div>
                <div className="card-body">
                  <div className="container">
                    <List items={this.phoneFeatures}/>
                  </div>
                </div>
                <div className="card-footer">
                  <button className="btn btn-link" style={{color: 'grey'}}>Add a card...</button>
                </div>
              </div>
            </div>
            <div className="col-md-3"/>
          </div>
        </div>
    )
  }

}

export default Trillo;
