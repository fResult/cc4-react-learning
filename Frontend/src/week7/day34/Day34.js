import React from 'react';
import {Typography} from 'antd'

const {Title} = Typography;

const Day34 = (props) => {
  return (
      <div>
        <Title level={4}>Day 34: {new Date('2019-12-6').toDateString()}</Title>
      </div>
  );
};

export default Day34;
