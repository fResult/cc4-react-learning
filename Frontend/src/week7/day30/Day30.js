import React from 'react';
import {Provider} from 'react-redux';
import {Typography} from 'antd'
import ReactReduxNoteApp from "./ReactReduxNoteApp";

import store from './react-redux-note-app/redux/store/store.js';

const {Title} = Typography;

const Day30 = (props) => {
  return (
      <>
        <Provider store={store}>
          <Title level={4}>Day 30: {new Date('2019-12-2').toDateString()}</Title>
          <ReactReduxNoteApp/>
        </Provider>
      </>
  );
};

export default Day30;
