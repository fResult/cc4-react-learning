import {combineReducers} from 'redux'
import notesReducer from './notesReducer'
import visibilityReducer from './visibilityReducer'

const rootReducer = combineReducers({
  notes: notesReducer,
  visibility: visibilityReducer
});

export default rootReducer
