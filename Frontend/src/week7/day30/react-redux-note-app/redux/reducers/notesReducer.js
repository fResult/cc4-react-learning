import {ADD_NOTE, REMOVE_NOTE, STATUS_INACTIVE} from "../actions/actions";

function notesReducer(notes = [], action) {
  const {id, title, content, status, createdDate, tag} = action;

  switch (action.type) {
    case ADD_NOTE:
      return [
        ...notes,
        {id, title, content, status,
          createdDate,
          tag,
        }
      ];
    case REMOVE_NOTE:
      return notes.map(note => note.id === action.id ? {...note, status: STATUS_INACTIVE} : note);
    default:
      return notes
  }
}

export default notesReducer;
