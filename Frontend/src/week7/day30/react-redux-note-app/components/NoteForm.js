import React from 'react'
import {connect} from 'react-redux';

import {Form, Input, Button, Typography, Select, Row, Col, DatePicker} from 'antd';

import {addNote} from '../redux/actions/actions'
import styles from './NoteForm.module.css';

const {Option} = Select;
const {TextArea} = Input;
const {Title, Text} = Typography;

class NoteForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      content: ''
    }
  }

  handleTitleChange = (e) => {
    this.setState({title: e.target.value})
  };

  handleContentChange = (e) => {
    this.setState({content: e.target.value})
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.addNote(this.state.title, this.state.content);
    this.setState({title: ''});
    this.setState({content: ''});
  };

  render() {
    return (
        <div>
          <div className={`container ${styles.Root}`}>
            <Row style={{width: '100%'}}>
              <Col xs={2} sm={9} md={6}/>

              <Col xs={20} sm={18} md={12} className={styles.MiddleColumn}>
                <Form onSubmit={this.handleSubmit} className={styles.NoteForm}>
                  <Title level={2}>Add a Note</Title>

                  <Form.Item label={<Text strong className={styles.TextLabel}>Title</Text>} colon required
                             extra={this.state.title === '' && <Text type="danger">Title must not be null.</Text>}
                  >
                    <Input allowClear size="large"
                           onChange={this.handleTitleChange}
                           value={this.state.title}
                    />
                  </Form.Item>

                  <Form.Item label={<Text strong className={styles.TextLabel}>Content</Text>} colon required
                             extra={this.state.content === '' && <Text type="danger">Content must not be null.</Text>}
                  >
                    <TextArea allowClear
                              onChange={this.handleContentChange}
                              value={this.state.content}
                              autoSize={{minRows: 5, maxRows: 5}}
                    />
                  </Form.Item>

                  <Form.Item label={<Text strong className={styles.TextLabel}>Tag</Text>} colon >
                    <Select defaultValue="Work">
                      <Option value="Work">Work</Option>
                      <Option value="Relationship">Relationship</Option>
                      <Option value="Financial">Financial</Option>
                    </Select>
                  </Form.Item>
                  <Form.Item label={<Text strong className={styles.TextLabel}>Due Date</Text>} colon>
                    <DatePicker format="DD/MM/YYYY" placeholder="DD/MM/YYYY" style={{width: '100%'}}/>
                  </Form.Item>
                  <Form.Item>
                    <Button type="primary" size="large" htmlType="submit"
                    disabled={this.state.title === '' || this.state.content === ''}>
                      Add Note
                    </Button>
                  </Form.Item>
                </Form>
              </Col>

              <Col xs={2} sm={9} md={6}/>
            </Row>
          </div>
        </div>
    )
  }
}

// Map Action Creator to be Props of component
const mapDispatchToProps = {
  addNote: addNote
};

export default connect(null, mapDispatchToProps)(NoteForm)
