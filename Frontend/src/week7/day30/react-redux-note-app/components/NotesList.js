import React from 'react'
import {connect} from 'react-redux'
import {Typography, Button, Card, Input, Icon} from 'antd';

import {removeNote, showActive, showInactive} from '../redux/actions/actions';

const {Paragraph, Text, Title} = Typography;

class NotesList extends React.Component {

  handleShowActiveNotes = () => {
    this.props.showActive();
  };

  handleShowInactiveNotes = () => {
    this.props.showInactive();
  };

  render() {
    const visibility = this.props.visibility;
    const notes = this.props.notes.filter(note => note.status === visibility);
    return (
        <>
          <Title level={2}>Notes</Title>
          <Input placeholder="Search Title or Content" suffix={<Icon type="search"/>}/>
          <Button.Group>
            <Button onClick={() => this.handleShowActiveNotes()}>Show active notes</Button>
            <Button onClick={() => this.handleShowInactiveNotes()}>Show inactive notes</Button>
          </Button.Group>
          <br/>
          <Button.Group>
            <Button>Work</Button>
            <Button>Relationship</Button>
            <Button>Financial</Button>
          </Button.Group>
          <div>{
            notes.map(note => (
                <Card key={note.id} title={<Title level={4} editable strong>{note.title}</Title>}
                      extra={<Button type="danger" onClick={() => this.props.deleteNote(note.id)}>X</Button>}
                      style={{width: 350}}
                >
                  <Text code>{note.tag}</Text>

                  <Paragraph editable>{note.content}</Paragraph>
                  <br/>
                  <span>{note.createdDate}</span>
                </Card>
            ))
          }</div>
        </>
    )
  }
}

const mapDispatchToProps = {
  deleteNote: removeNote,
  showInactive: showInactive,
  showActive: showActive
};

// Make State to be Props of component
const mapStateToProps = (state) => {
  return {
    notes: state.notes,
    visibility: state.visibility
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(NotesList)
