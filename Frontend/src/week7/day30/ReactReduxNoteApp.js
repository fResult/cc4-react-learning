import React, {Component} from 'react';
import NoteForm from "./react-redux-note-app/components/NoteForm";
import NotesList from "./react-redux-note-app/components/NotesList";

class ReactReduxNoteApp extends Component {
  render() {
    return (
        <>
          <h1>React-Redux Notes app</h1>

          <NoteForm/>
          <hr/>
          <NotesList/>
        </>
    );
  }
}

export default ReactReduxNoteApp;
