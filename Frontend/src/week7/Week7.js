import React from 'react';
import {Divider, Menu, Typography, Row, Col} from 'antd'
import {
  Switch,
  Route,
  Link,
  useRouteMatch,
  // useParams
} from "react-router-dom";
import Day30 from "./day30/Day30";
import Day31 from "./day31/Day31";
import Day32 from "./day32/Day32";
import Day33 from "./day33/Day33";
import Day34 from "./day34/Day34";

const {Title} = Typography;

const Week7 = (props) => {
  const match = useRouteMatch();

  return (
      <div>
        <div className="container-fluid">
          <Title level={3}>Week 7</Title>
          <Divider dashed="true"/>
          <Row>
            <Col span={6}>
          <Menu mode="vertical">
                <Menu.Item>
                  <Link to={`${match.url}/day30`}>Day 30: React-Redux Note App: </Link>
                </Menu.Item>
                <Menu.Item>
                  <Link to={`${match.url}/day31`}>Day 31: xxxxxxxxxxxxxxxxxxx: </Link>
                </Menu.Item>
                <Menu.Item>
                  <Link to={`${match.url}/day32`}>Day 32: xxxxxxxxxxxxxxxxxxx: </Link>
                </Menu.Item>
                <Menu.Item>
                  <Link to={`${match.url}/day33`}>Day 33: xxxxxxxxxxxxxxxxxxx: </Link>
                </Menu.Item>
                <Menu.Item>
                  <Link to={`${match.url}/day34`}>Day 34: xxxxxxxxxxxxxxxxxxx: </Link>
                </Menu.Item>
          </Menu>
          </Col>
          </Row>

          <Switch>
            <Route path={`${match.path}/day30`}><Day30/></Route>
            <Route path={`${match.path}/day31`}><Day31/></Route>
            <Route path={`${match.path}/day32`}><Day32/></Route>
            <Route path={`${match.path}/day33`}><Day33/></Route>
            <Route path={`${match.path}/day34`}><Day34/></Route>
          </Switch>
        </div>
      </div>
);
};

export default Week7;
