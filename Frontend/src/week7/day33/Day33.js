import React from 'react';
import {Typography} from 'antd'

const {Title} = Typography;

const Day33 = (props) => {
  return (
      <div>
        <Title level={4}>Day 33: {new Date('2019-12-5').toDateString()}</Title>
      </div>
  );
};

export default Day33;
