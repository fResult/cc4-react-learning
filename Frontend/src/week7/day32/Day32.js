import React from 'react';
import {Typography} from 'antd'

const {Title} = Typography;

const Day32 = (props) => {
  return (
      <div>
        <Title level={4}>Day 32: {new Date('2019-12-4').toDateString()}</Title>
      </div>
  );
};

export default Day32;
