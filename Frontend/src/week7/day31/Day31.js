import React from 'react';
import {Typography} from 'antd'

const {Title} = Typography;

const Day31 = (props) => {
  return (
      <div>
        <Title level={4}>Day 31: {new Date('2019-12-3').toDateString()}</Title>
      </div>
  );
};

export default Day31;
