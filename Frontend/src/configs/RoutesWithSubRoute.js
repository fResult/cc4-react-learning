import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export default function RouteWithSubRoute(route) {
  return (
      <Route path={route.path} render={props => (
          <route.component {...props} routes={route.routes}/>
      )}/>
  )
}

