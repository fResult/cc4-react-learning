import Week4 from "../week4/Week4";
import Week5 from "../week5/Week5";

import Day18 from "../week4/day18/Day18";
import Day19 from "../week4/day19/Day19";
import Day20 from "../week5/day20/Day20";
import Day21 from "../week5/day21/Day21";

const routes = [
  {
    path: "/week4",
    component: Week4,
    routes: [
      {
        path: '/week4/day18',
        component: Day18
      }, {
        path: '/week4/day19',
        component: Day19
      }
    ]
  }, {
    path: "/week5",
    component: Week5,
    routes: [
      {
        path: '/week5/day20',
        component: Day20
      }, {
        path: '/week5/day25',
        component: Day21
      }
    ]
  },
];

export default routes;
